/*
 * Implementation of the prediction equations of a simple kalman filter
 */

#define BLOCK_NAME "control/fake_trj"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <armadillo>
#include <ubx/ubx.h>
using namespace std;
using namespace arma;


#include "../../types/control_types/State.h"
#include "../../types/control_types/ControlAction.h"

char block_meta[] =
	"{ doc='Kalman filter - prediction equations',"
	"  real-time=true,"
	"}";

ubx_port_t block_ports[] = {
	{ .name = "control_out"	, .out_type_name  = "struct ControlAction" },
	{ NULL }
};

ubx_config_t block_config[] = {
	{ .name="filepath_U"	, .type_name = "char"		, .doc="TODO" }, //TODO doc
	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/* ================================   BLOCK INFO   ================================ */
struct block_info {
  vec			U		;
  ubx_port_t*		ubx_p_ctrl_out	;
  struct ControlAction	ubx_ctrl_out	;
};
/* ================================   R/W FUNCS   ================================ */
def_write_fun ( write_ctrl_out , struct ControlAction );
/* ================================   INIT   ================================ */
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
  out:
	return ret;
}
/* ================================   CLEANUP   ================================ */
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}
/* ================================   START   ================================ */
static int block_start(ubx_block_t *b)
{
	unsigned int  tmplen;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	char *chrptr;

	// Cache ports pointers
	inf->ubx_p_ctrl_out	= ubx_port_get ( b , "control_out"	);

	// Read config
	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_U", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_X0 is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->U).load(chrptr)  ;


	// Init state with zeros
	vec tmp_zeros;
	tmp_zeros.set_size ( (inf->U).n_elem );
	tmp_zeros.zeros();
	memcpy ( (inf->ubx_ctrl_out).data , tmp_zeros.memptr() , tmp_zeros.n_elem*sizeof(double) );
	(inf->ubx_ctrl_out).n_elem = tmp_zeros.n_elem;
	// Write out initial 0 
	write_ctrl_out ( inf->ubx_p_ctrl_out , &(inf->ubx_ctrl_out) );
	// Prepare real control data to write out in the step function
	memcpy ( (inf->ubx_ctrl_out).data , (inf->U).memptr() , (inf->U).n_elem*sizeof(double) );
	(inf->ubx_ctrl_out).n_elem = (inf->U).n_elem;

	return 0;
}

static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	// Data is already ready! Write out
	write_ctrl_out ( inf->ubx_p_ctrl_out , &(inf->ubx_ctrl_out) );
}

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
