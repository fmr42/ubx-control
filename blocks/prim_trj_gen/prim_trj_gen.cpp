/*
 * TODO doc
 */

#define BLOCK_NAME "copter/prim_trj_gen"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
//#include <armadillo>
#include <ubx/ubx.h>
#include "../../types/control_types/SetPoint.h"
#include "../../types/control_types/State.h"
#include <cmath>
#include <armadillo>
#include "../../include/arma_quat_lib.cpp"
using namespace std;
using namespace arma;
#include <mqueue.h>
/*========================   BLOCK METADATA   ====================================================*/
char block_meta[] =
	"{ doc='primitive trajectories generator'," //TODO
	"  real-time=true,"
	"}";

/*========================   BLOCK PORTS   =======================================================*/
ubx_port_t block_ports[] = {
	{ .name = "state_in"	 , .in_type_name  = "struct State" },
	{ .name = "setpoint_out" , .out_type_name  = "struct SetPoint" },
	{ NULL }
};

/*====================   BLOCK CONFIG   ====================*/
ubx_config_t block_config[] = {
	{ .name="dt"		, .type_name = "double", .doc="" },
	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/*==========================================		BLOCK DATA		==========================================*/
struct block_info {
	double duration;
	double t;
	double dt;
	double s;
	
	vec P0_offset;
	double yaw;
	int actuating_code;

	struct SetPoint	sp;	// cache memory
	struct State	stt;	// cache memory

	mqd_t mq;
};

// convenience functions for I/O
def_read_fun  ( read_stt , struct State		);
def_write_fun ( write_sp , struct SetPoint	);

/*==========================================		BLOCK INIT		==========================================*/
// This function allocate the memory required by this block
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
	
 out:
	return ret;
}

/*==========================================		BLOCK CLEANUP		==========================================*/
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}

/*==========================================		BLOCK START		==========================================*/
// This function read the block config
static int block_start(ubx_block_t *b)
{
	unsigned int  len;
	int ret;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	//TODO check for errors (if ( radious==0 )
	ret=0;

	inf->dt = *((double*) ubx_config_get_data_ptr(b, "dt"		, &len));
	inf->t = 0 ;
	inf->s = 0 ;
	inf->duration = 1 ;

	inf->actuating_code = 0 ;
	inf->yaw = 0 ;

	inf->P0_offset = zeros<vec>(3);

// Set point at current position
read_stt ( ubx_port_get(b,"state_in") , &(inf->stt) );

/* // This set the initial sp to the current copter position
(inf->sp).data[0]	= (inf->stt).data[0];
(inf->sp).data[1]	= (inf->stt).data[1];
(inf->sp).data[2]	= (inf->stt).data[2];
inf->P0_offset(0) =(inf->stt).data[0];
inf->P0_offset(1) =(inf->stt).data[1];
inf->P0_offset(2) =(inf->stt).data[2];
*/

(inf->sp).data[0]	= 0;
(inf->sp).data[1]	= 0;
(inf->sp).data[2]	= 0;
inf->P0_offset(0) =0;
inf->P0_offset(1) =0;
inf->P0_offset(2) =0;



// 0 speed
(inf->sp).data[3]=0 ;
(inf->sp).data[4]=0 ;
(inf->sp).data[5]=0 ;
//0 acc
(inf->sp).data[6]=0 ;
(inf->sp).data[7]=0 ;
(inf->sp).data[8]=0 ;
// q
(inf->sp).data[9]=1 ;
(inf->sp).data[10]=0 ;
(inf->sp).data[11]=0 ;
(inf->sp).data[12]=0 ;
// d/dt(q)
(inf->sp).data[13]=0 ;
(inf->sp).data[14]=0 ;
(inf->sp).data[15]=0 ;
(inf->sp).data[16]=0 ;
// d/dt ( d/dt (q) )
(inf->sp).data[17]=0 ;
(inf->sp).data[18]=0 ;
(inf->sp).data[19]=0 ;
(inf->sp).data[20]=0 ;
(inf->sp).data[21]=0 ;
(inf->sp).n_elem=21 ;

write_sp ( ubx_port_get ( b , "setpoint_out" )	, &inf->sp);




  struct mq_attr ma;		// message queue attributes
  ma.mq_flags = 0;		// blocking read/write
  ma.mq_maxmsg = 10;		// maximum number of messages allowed in queue
  ma.mq_msgsize = sizeof(int);	// messages are contents of an int
  ma.mq_curmsgs = 0;		// number of messages currently in queue
  inf->mq = mq_open("/test", O_RDWR | O_CREAT | O_NONBLOCK, 0700, &ma);

  if (inf->mq == -1) {
    printf("Failed to create queue.\n");
    //return(-1);
  }



	goto out;

 error:
	free(inf);
 out:
	return ret;

}

/*==========================================		BLOCK STEP		==========================================*/
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	// Position
	vec P0 = zeros<vec>(3);
	vec P1 = zeros<vec>(3);
	vec P2 = zeros<vec>(3);

	double scale_space = 10 ;
	double scale_time  = 5 ;

	int msg=-1;
	mq_receive(inf->mq, (char*)&msg, sizeof(int), NULL);
	vec dir = zeros<vec>(3);
	if (msg > 0 ) {											//NEW MESSAGE
		switch ( msg ) {
		case (1) :
			dir(0) = 1		; //North
			inf->actuating_code = 1	;
			break ;
		case (2) :
			inf->actuating_code = 2	;
			dir(0) = -1		; //South
			break ;
		case (3) :
			inf->actuating_code = 3	;
			dir(1) = -1		; //west
			break ;
		case (4) :
			inf->actuating_code = 4	;
			dir(1) = 1		; //east
			break ;
		case (5) :
			inf->actuating_code = 5	;
			dir(2) = -1		; //up
			break ;
		case (6) :
			inf->actuating_code = 6	;
			dir(2) = 1		; //down
			break ;
		case (7) :
			inf->yaw += 90	;
			inf->yaw = (int)inf->yaw % 360	;
			return; // TODO Not the correct implementation! Should be smooth
			break ;
		case (8) :
			inf->yaw += 270			;
			inf->yaw = (int)inf->yaw % 360	;
			return; //TODO Not the correct implementation! Should be smooth
			break ;
		default :
			cout << "Error!!! msg unknown\n" ;
			break ;
		}
		
		// start new trajectory
		read_stt ( ubx_port_get ( b , "state_in" )	, &(inf->stt) );
		inf->P0_offset(0) = (inf->stt).data[0]		;
		inf->P0_offset(1) = (inf->stt).data[1]		;
		inf->P0_offset(2) = (inf->stt).data[2]		;
		P0 = inf->P0_offset;
		inf->t=0;
		P1 = dir * scale_space/scale_time;
	} else if (msg==0) {
		read_stt ( ubx_port_get ( b , "state_in" )	, &(inf->stt) );
		P0(0) = (inf->stt).data[0]	;
		P0(1) = (inf->stt).data[1]	;
		P0(2) = (inf->stt).data[2]	;
		P1.zeros()			;
		P2.zeros()			;
		inf->P0_offset = P0			;
		inf->actuating_code=0			;
	} else if ( msg < 0 ) { 							// NO NEW MESSAGES!!
		switch ( inf->actuating_code ) {
		case (1) :
			dir(0) = 1		; //North
			break ;
		case (2) :
			dir(0) = -1		; //South
			break ;
		case (3) :
			dir(1) = -1		; //west
			break ;
		case (4) :
			dir(1) = 1		; //east
			break ;
		case (5) :
			dir(2) = -1		; //up
			break ;
		case (6) :
			dir(2) = 1		; //down
			break ;
		case (0) :
			break ;
		default :
			cout << "Unknown actuating code" << endl;
			break ;
		}								// CONT NORTH

		if ( inf->t < scale_time ) {
			inf->t = inf->t + inf->dt;
			P0 = inf->P0_offset + dir * ( inf->t / scale_time ) * scale_space		;
			P1 = dir*scale_space/scale_time							;
		} else if ( inf->t >= scale_time) {							// STOP NORTH
			inf->actuating_code = 0						;
			inf->P0_offset = inf->P0_offset + dir * scale_space	;
			P0=inf->P0_offset;
			P1.zeros();
			P2.zeros();
		}
	}


//==================================== Compute rotation reference

	vec Q0 = zeros<vec>(4);	Q0(0) = 1;
	vec Q1 = zeros<vec>(4);
	vec Q2 = zeros<vec>(4);

	// Compute rotation matrix
	mat Rr   = zeros<mat>(3,3);
	vec e3   = zeros<vec>(3) ; e3(2) = 1 ;
	vec port = zeros<vec>(3) ;
	vec heading = zeros<vec>(3) ;

#define G 9.81


	heading(0) =   cos( 2*datum::pi*inf->yaw / 360);
	heading(1) =   sin( 2*datum::pi*inf->yaw / 360);
	heading(2) =   0 ;

	Rr.col(2) = normalise( G*e3 - P2 )				;	// z-axis aligned with the accelleration vector
	Rr.col(1) = normalise(( cross ( Rr.col(2) , heading 	)))	;
	Rr.col(0) = normalise(( cross ( Rr.col(1) , Rr.col(2)	)))	;

	Q0 = arma_mat_to_q(Rr);

#ifndef UBX_NO_DEBUG
	// Set precision
	streamsize ss = std::cout.precision();
	cout.precision(3);
	// Dump
	cout << "========   " << b->name << "   ========" << endl ;
	cout << "t   : "  << inf->t << endl   ;
	cout << "P0  : "  << P0.t()			;
	cout << "P1  : "  << P1.t()			;
	cout << "P2  : "  << P2.t()			;
	cout << "Rr  : "  << endl			;
	Rr.raw_print(std::cout)				;
	cout << "Q0  : " << endl			;
	Q0.raw_print(std::cout)				;
	cout << endl;
	// Restore precision
	cout.precision (ss);
#endif

	inf->sp.data[0] = P0(0);
	inf->sp.data[1] = P0(1);
	inf->sp.data[2] = P0(2);

	inf->sp.data[3] = P1(0);
	inf->sp.data[4] = P1(1);
	inf->sp.data[5] = P1(2);

	inf->sp.data[6] = P2(0);
	inf->sp.data[7] = P2(1);
	inf->sp.data[8] = P2(2);

	inf->sp.data[9]  = Q0(0);
	inf->sp.data[10] = Q0(1);
	inf->sp.data[11] = Q0(2);
	inf->sp.data[12] = Q0(3);

	inf->sp.data[13] = Q1(0);
	inf->sp.data[14] = Q1(1);
	inf->sp.data[15] = Q1(2);
	inf->sp.data[16] = Q1(3);

	inf->sp.data[17] = Q2(0);
	inf->sp.data[18] = Q2(1);
	inf->sp.data[19] = Q2(2);
	inf->sp.data[20] = Q2(3);

	inf->sp.n_elem=21;

	//TODO set metadata
	//TODO cache port
	write_sp ( ubx_port_get ( b , "setpoint_out" ) , &inf->sp);

	inf->t = inf->t + inf->dt;
}

/*==========================================		BUILD BLOCK		==========================================*/

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
