/*
 * Read the state of the copter and output the linearized model
 */

#define BLOCK_NAME "copter/kf_predict_copter"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <armadillo>
#include <ubx/ubx.h>
using namespace std;
using namespace arma;


#include "../../types/control_types/ControlAction.h"
#include "../../types/control_types/State.h"
#include "../../include/arma_quat_lib.cpp"


char block_meta[] =
	"{ doc='Read copter state and returns matrices A and B',"
	"  real-time=true,"
	"}";

ubx_port_t block_ports[] = {
	{ .name = "stt_in"	, .in_type_name		= "struct State"		},
	{ .name = "ctrl_in"	, .in_type_name		= "struct ControlAction"	},
	{ .name = "stt_out"	, .out_type_name	= "struct State"		},
	{ NULL }
};

ubx_config_t block_config[] = {
	{ .name="mass"		, .type_name = "double"		, .doc="TODO" }, //TODO doc
	{ .name="J_xx"		, .type_name = "double"		, .doc="TODO" }, //TODO doc
	{ .name="J_yy"		, .type_name = "double"		, .doc="TODO" }, //TODO doc
	{ .name="J_zz"		, .type_name = "double"		, .doc="TODO" }, //TODO doc
	{ .name="process_noise"	, .type_name = "double"		, .doc="TODO" }, //TODO doc
	{ .name="dt"		, .type_name = "double"		, .doc="TODO" }, //TODO doc
	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/* ================================   BLOCK INFO   ================================ */
struct block_info {
	// Copter data
	double mass;
	double grav_acc;
	mat J;
	double dt;
	// Control Action
	double	Uf;
	vec	Ut;
	// Copter state old and new
	vec P0_old;	// old position
	vec P1_old;	// old velocity
	vec P2_old;	// old accelleration
	vec Q0_old;	// old angular position
	vec Q1_old;	// old angular velocity
	vec Q2_old;	// old angular accelleration

	vec W0_old;
	vec W0_new;
	vec W1_old;

	vec P0_new;
	vec P1_new;
	vec Q0_new;
	vec Q1_new;

	// constants
	vec e3		;


	// Kalman filter related vars
	mat P_old;	// Old cov. mat
	mat P_new;	// New cov. mat
	mat A;		// state transiction mat
	mat Q;		// process noise

	// ubx related vars
	ubx_port_t*		ubx_p_stt_in	;
	ubx_port_t*		ubx_p_ctrl_in	;
	ubx_port_t*		ubx_p_stt_out	;

	struct State		ubx_stt_in	;
	struct ControlAction	ubx_ctrl_in	;
	struct State		ubx_stt_out	;
	
};
/* ================================   R/W FUNCS   ================================ */
def_read_fun	( read_stt_in	, struct State		);
def_read_fun	( read_ctrl_in	, struct ControlAction	);
def_write_fun	( write_stt_out	, struct State		);
/* ================================   INIT   ================================ */
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
  out:
	return ret;
}
/* ================================   CLEANUP   ================================ */
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}
/* ================================   START   ================================ */
static int block_start(ubx_block_t *b)
{
	unsigned int  tmplen;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	char *chrptr;
	// ___ Cache ports pointers ___
	inf->ubx_p_stt_in	= ubx_port_get ( b , "stt_in"	);
	inf->ubx_p_ctrl_in	= ubx_port_get ( b , "ctrl_in"	);
	inf->ubx_p_stt_out	= ubx_port_get ( b , "stt_out"	);
	// ___ Read config ___
	inf->mass = *(double*) ubx_config_get_data_ptr(b, "mass", &tmplen);
	if (inf->mass <= 0) {
		ERR("%s: mass must be greater than zero", b->name);
		block_cleanup(b);
		return -1;
	}
	inf->J = zeros<mat>(3,3);
	(inf->J)(0,0) = *(double*) ubx_config_get_data_ptr(b, "J_xx", &tmplen);
	(inf->J)(1,1) = *(double*) ubx_config_get_data_ptr(b, "J_yy", &tmplen);
	(inf->J)(2,2) = *(double*) ubx_config_get_data_ptr(b, "J_zz", &tmplen);
	if ( (inf->J)(0,0)<=0 || (inf->J)(1,1)<=0 || (inf->J)(2,2)<=0 ) {
		ERR("%s: inertia must be greater than 0", b->name);
		block_cleanup(b);
		return -1;
	}
	inf->dt = *(double*) ubx_config_get_data_ptr(b, "dt", &tmplen);
	if (inf->dt<=0) {
		ERR("%s: dt must be greater than zero", b->name);
		block_cleanup(b);
		return -1;
	}
	// ___ Init vars size ___
	inf->P0_old = zeros<vec>(3)	;	
	inf->P1_old = zeros<vec>(3)	;
	inf->Q0_old = zeros<vec>(4)	;
	inf->Q1_old = zeros<vec>(4)	;
	inf->W0_new = zeros<vec>(4)	;
	inf->W0_old = zeros<vec>(4)	;
	inf->W1_old = zeros<vec>(4)	;
	inf->P0_new = zeros<vec>(3)	;
	inf->P1_new = zeros<vec>(3)	;
	inf->Q0_new = zeros<vec>(4)	;	inf->Q0_new(0) = 0 ; inf->Q0_new(3) = 1 ;
	inf->Q1_new = zeros<vec>(4)	;

	inf->P_new = zeros<mat>(21,21)	;
	inf->P_old = zeros<mat>(21,21)	;

	inf->Uf = 0		;
	inf->Ut = zeros<vec>(3)	;

	// ___ Init constants ___
	inf->e3 = zeros<vec>(3)	; inf->e3(2) = 1 ;
	inf->grav_acc = 9.81	;

	// ___ Build state transiction matrix ___
	inf->A = zeros<mat>(21,21);	
	// P0
	(inf->A).submat(0,0,2,2)	= eye<mat>(3,3)			;
	(inf->A).submat(0,3,2,5)	= eye<mat>(3,3) * inf->dt	;
	// P1	
	(inf->A).submat(3,3,5,5)	= eye<mat>(3,3)			;
	// Q0
	(inf->A).submat(6,6,9,9)	= eye<mat>(4,4)			;
	(inf->A).submat(6,10,9,13)	= eye<mat>(4,4) * inf->dt	;
	// Q1
	(inf->A).submat(10,10,13,13)	= eye<mat>(4,4)			;

	// ___ Build process noise mat ___
	(inf->Q) = eye<mat>(21,21) * *(double*) ubx_config_get_data_ptr(b, "process_noise", &tmplen)	;
	// Init state

	(inf->ubx_stt_out).n_elem = 21;
	for ( int i = 0 ; i < 3 ; i++ ) {
	  ( inf->ubx_stt_out ).data [ i   ] = (inf->P0_new)( i )		;
	  ( inf->ubx_stt_out ).data [ i+3 ] = (inf->P1_new)( i )		;
	 // ( inf->ubx_stt_out ).data [ i+3 ] = (inf->P1_new)( i )		;
	}
	for ( int i = 0 ; i < 4 ; i++ ) {
	  ( inf->ubx_stt_out ).data[ i+9  ] = (inf->Q0_new)( i )		;
	  ( inf->ubx_stt_out ).data[ i+13 ] = (inf->Q1_new)( i )		;
	  ( inf->ubx_stt_out ).data[ i+17 ] = (inf->Q1_new)( i )		;
	}
	for ( int i = 0 ; i < 21 ; i++ ) {
	  for ( int j = 0 ; j < 21 ; j++ ) {
	    ( inf->ubx_stt_out ).covariance[ i*21+j ] = (inf->P_new)(i,j)	;
	  }
	}

	write_stt_out ( inf->ubx_p_stt_out , &(inf->ubx_stt_out) );

	return 0;
}
/* ========================================= STEP ======================================================*/
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	/* === Read the state === */
	// TODO speedup the following step: it should be possible
	// to avoid the data copying
	read_stt_in ( inf->ubx_p_stt_in , &(inf->ubx_stt_in) );
	if ( (inf->ubx_stt_in).n_elem != 21 ) {
	  ERR("%s: invalid copter state", b->name ) ;
	  // TODO take a decision in case of error
	  return ;
	}
	for ( int i = 0 ; i < 3 ; i++ ) {
	  (inf->P0_old)( i ) = ( inf->ubx_stt_in ).data [ i   ];
	  (inf->P1_old)( i ) = ( inf->ubx_stt_in ).data [ i+3 ];
	  //(inf->P2_old)( i ) = ( inf->ubx_stt_in ).data [ i+6 ];
	}
	for ( int i = 0 ; i < 4 ; i++ ) {
	  (inf->Q0_old)( i ) = ( inf->ubx_stt_in ).data[ i+9  ];
	  (inf->Q1_old)( i ) = ( inf->ubx_stt_in ).data[ i+13 ];
//	  (inf->Q1_old)( i ) = ( inf->ubx_stt_in ).data[ i+10 ];
	}
	for ( int i = 0 ; i < 21 ; i++ ) {
	  for ( int j = 0 ; j < 21 ; j++ ) {
	    (inf->P_old)(i,j) = ( inf->ubx_stt_in ).covariance[ i*21+j ];
	  }
	}

	/* === Read control action === */
	// TODO speedup the following step: it should be possible
	// to avoid the data copying
	read_ctrl_in ( inf->ubx_p_ctrl_in , &(inf->ubx_ctrl_in) );
	if ( (inf->ubx_ctrl_in).n_elem != 4 ) {
	  ERR("%s: invalid copter ctrl action", b->name ) ;
	  // TODO take a decision in case of error
	  return ;
	}
	inf->Uf		= (inf->ubx_ctrl_in).data[0];
	inf->Ut(0)	= (inf->ubx_ctrl_in).data[1];
	inf->Ut(1)	= (inf->ubx_ctrl_in).data[2];
	inf->Ut(2)	= (inf->ubx_ctrl_in).data[3];


	/* === update linear state === */
	inf->P2_old = - inf->Uf * ( arma_rodriguez(inf->Q0_old) * inf->e3 ) / inf->mass + inf->grav_acc * inf->e3 	;
	inf->P0_new = inf->P0_old + inf->P1_old * inf->dt + 0.5 * inf->P2_old * inf->dt * inf->dt			;
	inf->P1_new = inf->P1_old + inf->P2_old * inf->dt								;
	
	/* === Compute angular velocity and accelleration === */
	(inf->W0_old).subvec(1,3) = arma_quat_d_to_vel(inf->Q0_old,inf->Q1_old) ;
	(inf->W1_old).subvec(1,3) = (inf->J).i() * ( cross((inf->J) * ( inf->W0_old).subvec(1,3),( inf->W0_old).subvec(1,3)) + inf->Ut  );
	inf->W0_new = inf->W0_old + inf->W1_old * inf->dt ;
	/* === Update quaternions === */
	// TODO improve quaternion prediction
	inf->Q2_old = 0.5 * arma_quat_hamilton ( inf->Q1_old , inf->W0_old ) + 0.5 * arma_quat_hamilton ( inf->Q0_old , inf->W1_old ) ;
	inf->Q0_new = normalise( inf->Q0_old + inf->Q1_old *inf->dt + 0.5 * inf->Q2_old * inf->dt * inf->dt ) ;
	inf->Q1_new = inf->Q1_old + inf->Q2_old * inf->dt;

	/* === update covariance matrix === */
	inf->P_new = inf->A * inf->P_old * (inf->A).t() + inf->Q			;
	
#ifndef UBX_NO_DEBUG
cout << "======== " << b->name << " debug dump ========" << endl	;
cout << "P0_old = " << inf->P0_old.t() ;
cout << "P1_old = " << inf->P1_old.t() ;
cout << "P2_old = " << inf->P2_old.t() ;

cout << "Q0_old = " << inf->Q0_old.t() ;
cout << "Q1_old = " << inf->Q1_old.t() ;
cout << "Q2_old = " << inf->Q2_old.t() ;

cout << "P0_new = " << inf->P0_new.t() ;
cout << "P1_new = " << inf->P1_new.t() ;
cout << "P2_new = " << inf->P2_new.t() ;

cout << "Q0_new = " << inf->Q0_new.t() ;
cout << "Q1_new = " << inf->Q1_new.t() ;
cout << "Q2_new = " << inf->Q2_new.t() ;
#endif

	/* === Write new state === */
	(inf->ubx_stt_out).n_elem = 21 ;
	for ( int i = 0 ; i < 3 ; i++ ) {
	  ( inf->ubx_stt_out ).data [ i   ] = (inf->P0_new)( i )		;
	  ( inf->ubx_stt_out ).data [ i+3 ] = (inf->P1_new)( i )		;
	  //( inf->ubx_stt_out ).data [ i+6 ] = (inf->P2_new)( i )		;
	}
	for ( int i = 0 ; i < 4 ; i++ ) {
	  ( inf->ubx_stt_out ).data[ i+9  ] = (inf->Q0_new)( i )		;
	  ( inf->ubx_stt_out ).data[ i+13 ] = (inf->Q1_new)( i )		;
	  //( inf->ubx_stt_out ).data[ i+17 ] = (inf->Q2_new)( i )		;
	}
	for ( int i = 0 ; i < 21 ; i++ ) {
	  for ( int j = 0 ; j < 21 ; j++ ) {
	    ( inf->ubx_stt_out ).covariance[ i*21+j ] = (inf->P_new)(i,j)	;
	  }
	}
	write_stt_out ( inf->ubx_p_stt_out , &(inf->ubx_stt_out) );
	return ;
}

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
