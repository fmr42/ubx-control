/*
 * TODO doc
 */

#define BLOCK_NAME "copter/circular_trj_copter"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
//#include <armadillo>
#include <ubx/ubx.h>
#include "../../types/control_types/SetPoint.h"
#include <cmath>
#include <armadillo>
#include "../../include/arma_quat_lib.cpp"
using namespace std;
using namespace arma;

/*========================   BLOCK METADATA   ====================================================*/
char block_meta[] =
	"{ doc='Circular trajectory generator'," //TODO
	"  real-time=true,"
	"}";

/*========================   BLOCK PORTS   =======================================================*/
ubx_port_t block_ports[] = {
	{ .name = "setpoint"	, .out_type_name  = "struct SetPoint" },
	{ NULL }
};

/*====================   BLOCK CONFIG   ====================*/
ubx_config_t block_config[] = {
	{ .name="radius"	, .type_name = "double", .doc="" },
	{ .name="altitude"	, .type_name = "double", .doc="" },
	{ .name="yaw"		, .type_name = "double", .doc="" },
	{ .name="speed"		, .type_name = "double", .doc="" },
	{ .name="dt"	    	, .type_name = "double", .doc="" },
	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/*==========================================		BLOCK DATA		==========================================*/
struct block_info {
 	// Timer
	//Timer tmr;
	double radious;
	double altitude;
	double yaw;
	double speed;

	double omega;
	
	double dt;
	double t;

	struct SetPoint sp; // cache memory
};

// convenience functions for I/O
def_write_fun  ( write_sp	, struct SetPoint	);

/*==========================================		BLOCK INIT		==========================================*/
// This function allocate the memory required by this block
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
	
 out:
	return ret;
}

/*==========================================		BLOCK CLEANUP		==========================================*/
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}

/*==========================================		BLOCK START		==========================================*/
// This function read the block config
static int block_start(ubx_block_t *b)
{
	unsigned int  len;
	int ret;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	//TODO check for errors (if ( radious==0 )
	ret=0;
	inf->radious	= *((double*) ubx_config_get_data_ptr(b, "radius"	, &len));
	inf->altitude	= *((double*) ubx_config_get_data_ptr(b, "altitude"	, &len));
	inf->yaw	= *((double*) ubx_config_get_data_ptr(b, "yaw"		, &len));
	inf->speed	= *((double*) ubx_config_get_data_ptr(b, "speed"	, &len));
	inf->dt		= *((double*) ubx_config_get_data_ptr(b, "dt"		, &len));
	
	if ( inf->radious == 0 ) {
	 inf->omega=0;
	} else {
	 inf->omega = inf->speed / inf->radious;
	}
	
	inf->t = 0 ;
	goto out;

 error:
	free(inf);
 out:
	return ret;

}

/*==========================================		BLOCK STEP		==========================================*/
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	// Position
	vec P0 = zeros<vec>(3);
	vec P1 = zeros<vec>(3);
	vec P2 = zeros<vec>(3);
	vec Q0 = zeros<vec>(4);
	vec Q1 = zeros<vec>(4);
	vec Q2 = zeros<vec>(4);
	P0(0) = inf->radious * cos( inf->omega * inf->t ) ;	
	P0(1) = inf->radious * sin( inf->omega * inf->t ) ;
	P0(2) = inf->altitude ;
	// Velocity
	P1(0) = - inf->radious * sin( inf->omega * inf->t ) * inf->omega ;	
	P1(1) =   inf->radious * cos( inf->omega * inf->t ) * inf->omega ;
	P1(2) = 0 ;
	// Accelleration
	P2(0) = - inf->radious * cos( inf->omega * inf->t ) * inf->omega * inf->omega ;	
	P2(1) = - inf->radious * sin( inf->omega * inf->t ) * inf->omega * inf->omega ;
	P2(2) = 0 ;

	// Compute rotation matrix
	mat Rr   = zeros<mat>(3,3);
	vec e3   = zeros<vec>(3) ; e3(2) = 1 ;
	vec port = zeros<vec>(3) ;
	vec heading = zeros<vec>(3) ;

#define G 9.81
#define PI 3.1416
	heading(0)=cos( 2*PI*inf->yaw / 360)	;
	heading(1)=sin( 2*PI*inf->yaw / 360)	;
	heading(2)=0			;
	port = cross ( e3 , heading )	;
	
	Rr.col(2) = G*e3 - P2				;
	Rr.col(2)=normalise( Rr.col(2) ) ;
	Rr.col(0) = cross ( port , Rr.col(2) )		;
	Rr.col(0)=normalise( Rr.col(0) ) ;
	Rr.col(1) = cross ( Rr.col(2) , Rr.col(0) )	;
	Rr.col(1)=normalise( Rr.col(1) ) ;

	Q0 = arma_mat_to_q(Rr);

#ifndef UBX_NO_DEBUG
	// Set precision
	streamsize ss = std::cout.precision();
	cout.precision(3);
	// Dump
	cout << "========   " << b->name << "   ========" << endl ;
	cout << "t   : "  << inf->t << endl   ;
	cout << "P0  : "  << P0.t()			;
	cout << "P1  : "  << P1.t()			;
	cout << "P2  : "  << P2.t()			;
	cout << "Rr  : "  << endl			;
	Rr.raw_print(std::cout)				;
	cout << "Q0  : " << endl			;
	Q0.raw_print(std::cout)				;
	cout << endl;
	// Restore precision
	cout.precision (ss);
#endif

	inf->sp.data[0] = P0(0);
	inf->sp.data[1] = P0(1);
	inf->sp.data[2] = P0(2);

	inf->sp.data[3] = P1(0);
	inf->sp.data[4] = P1(1);
	inf->sp.data[5] = P1(2);

	inf->sp.data[6] = P2(0);
	inf->sp.data[7] = P2(1);
	inf->sp.data[8] = P2(2);

	inf->sp.data[9]  = Q0(0);
	inf->sp.data[10] = Q0(1);
	inf->sp.data[11] = Q0(2);
	inf->sp.data[12] = Q0(3);

	inf->sp.data[13] = Q1(0);
	inf->sp.data[14] = Q1(1);
	inf->sp.data[15] = Q1(2);
	inf->sp.data[16] = Q1(3);

	inf->sp.data[17] = Q2(0);
	inf->sp.data[18] = Q2(1);
	inf->sp.data[19] = Q2(2);
	inf->sp.data[20] = Q2(3);

	inf->sp.n_elem=21;

	//TODO set metadata
	//TODO cache port
	write_sp ( ubx_port_get ( b , "setpoint" ) , &inf->sp);

	inf->t = inf->t + inf->dt;
}

/*==========================================		BUILD BLOCK		==========================================*/

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
