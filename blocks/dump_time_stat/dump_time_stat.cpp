
#define BLOCK_NAME "control/dump_time_stat"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <ubx/ubx.h>
using namespace std;

#include "../../include/common_functions.hpp"

/*========================   BLOCK METADATA   ====================================================*/
char block_meta[] =
	"{ doc='Compute time statitics when triggered'," //TODO
	"  real-time=true,"
	"}";
/*========================   BLOCK PORTS   =======================================================*/
ubx_port_t block_ports[] = {
	{ NULL }
};
/*====================   BLOCK CONFIG   ====================*/
ubx_config_t block_config[] = {
	{ .name="period"  	, .type_name = "unsigned int"	, .doc=""	},
	{ .name="update_rate" 	, .type_name = "double"		, .doc="" 	},
	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/*==========================================		BLOCK DATA		==========================================*/
struct block_info {
	unsigned int counter;
	unsigned int period;
	double update_rate;
	Timer tmr;
	double mean_time;

};

/*==========================================		BLOCK INIT		==========================================*/
// This function allocate the memory required by this block
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
 out:
	return ret;
}

/*==========================================		BLOCK CLEANUP		==========================================*/
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}

/*==========================================		BLOCK START		==========================================*/
// This function read the block config
static int block_start(ubx_block_t *b)
{
	unsigned int  len;
	//char *chrptr;

	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	inf->period	 = *((unsigned int*)	ubx_config_get_data_ptr ( b , "period"		, &len ) ) ;
	inf->update_rate = *((double*)		ubx_config_get_data_ptr ( b , "update_rate"	, &len ) ) ;
	inf->counter	 = 0	;

	(inf->tmr).reset();
	inf->mean_time=0;
	return 0;
}

/*==========================================		BLOCK STEP		==========================================*/
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	
	inf->counter++;
	inf->mean_time = ( 1 - inf->update_rate ) * inf->mean_time + ( inf->update_rate ) * (inf->tmr).elapsed() ;
	(inf->tmr).reset() ;
	
	if ( inf->counter >= inf->period ) {
	  inf->counter = 0 ;
	  cout << "Mean time = " << inf->mean_time << endl ;
	}

	return;
}

/*==========================================		BUILD BLOCK		==========================================*/

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
