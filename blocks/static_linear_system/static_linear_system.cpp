/*
 * Implementation of the prediction equations of a simple kalman filter
 */

#define BLOCK_NAME "control/static_linear_system"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <armadillo>
#include <ubx/ubx.h>
using namespace std;
using namespace arma;


#include "../../types/control_types/LinearSystem.h"

char block_meta[] =
	"{ doc='Read A and B from files and output a linear system',"
	"  real-time=true,"
	"}";

ubx_port_t block_ports[] = {
	{ .name = "sys_out"	, .out_type_name  = "struct LinearSystem" },
	{ NULL }
};

ubx_config_t block_config[] = {
	{ .name="filepath_A"	, .type_name = "char"		, .doc="TODO" }, //TODO doc
	{ .name="filepath_B"	, .type_name = "char"		, .doc="TODO" }, //TODO doc
	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/* ================================   BLOCK INFO   ================================ */
struct block_info {
  mat			A		;
  mat			B		;
  ubx_port_t*		ubx_p_sys_out	;
  struct LinearSystem	ubx_sys_out	;
};
/* ================================   R/W FUNCS   ================================ */
def_write_fun ( write_sys_out , struct LinearSystem );
/* ================================   INIT   ================================ */
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
  out:
	return ret;
}
/* ================================   CLEANUP   ================================ */
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}
/* ================================   START   ================================ */
static int block_start(ubx_block_t *b)
{
	unsigned int  tmplen;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	char *chrptr;

	// Cache ports pointers
	inf->ubx_p_sys_out	= ubx_port_get ( b , "sys_out"	);

	// Read config
	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_A", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_A is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->A).load(chrptr)  ;

	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_B", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_B is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->B).load(chrptr)  ;

	// TODO check matrices sizes
	memcpy ( (inf->ubx_sys_out).A.data , (inf->A).memptr() , (inf->A).n_rows*(inf->A).n_cols*sizeof(double) );
	(inf->ubx_sys_out).A.n_cols = (inf->A).n_rows;
	(inf->ubx_sys_out).A.n_rows = (inf->A).n_cols;

	memcpy ( (inf->ubx_sys_out).B.data , (inf->B).memptr() , (inf->B).n_rows*(inf->B).n_cols*sizeof(double) );
	(inf->ubx_sys_out).B.n_cols = (inf->B).n_cols;
	(inf->ubx_sys_out).B.n_rows = (inf->B).n_rows;

	// TODO Should next line be executed in "start"?
	write_sys_out ( inf->ubx_p_sys_out , &(inf->ubx_sys_out) );

	return 0;
}

static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	write_sys_out ( inf->ubx_p_sys_out , &(inf->ubx_sys_out) );
}

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
