/*
 * Dump a trajectory
 */

#define BLOCK_NAME "control/dump_ctrl"

#define DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <ubx/ubx.h>

#include "../../types/control_types/ControlAction.h"

char block_meta[] =
	"{ doc='Control action dump dump',"
	"  real-time=false,"
	"}";

static int block_init(ubx_block_t *b)
{
	return 0;
}

static void block_cleanup(ubx_block_t *b)
{
}

static void block_write(ubx_block_t *b, ubx_data_t* data) {
	int i;
	struct ControlAction *ctrl ;
	ctrl = data->data  ;

	printf (   "====== CtrlDump ======\n");
	for ( i = 0 ; i < ctrl->n_elem ; i++ ) 
	  printf ("%f " , ctrl->data[i] );
	printf ( "\n======================\n");
	
	return;
}

/* put everything together */
ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_INTERACTION,
	.meta_data = block_meta,
	.write = block_write,
	.init = block_init,
	.cleanup = block_cleanup,
};

static int mod_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void mod_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

UBX_MODULE_INIT(mod_init)
UBX_MODULE_CLEANUP(mod_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


