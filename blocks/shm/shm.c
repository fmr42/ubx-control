/*
 * An interaction block that sends data via a shm
 */

#define DEBUG 1

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <semaphore.h>
#include "ubx.h"
char shm_meta[] =
	"{ doc='shm interaction',"
	"  real-time=true,"
	"}";

ubx_config_t shm_config[] = {
	{ .name="shm_name", .type_name = "char" },
	{ .name="type_name", .type_name = "char", .doc="name of registered microblx type to transport" },
	{ .name="array_len", .type_name = "uint32_t", .doc="array length (multiplier) of data (default: 1)" },
	
	{ NULL }
};

struct shm_info {
	char *shm_name;
	int map_descriptor;
	const ubx_type_t* data_type;		/* type of contained elements */
	unsigned int data_len;		/* number of elements */
	void *map;
	sem_t *mutex;
};


static int shm_init(ubx_block_t *i)
{
	int ret=-1;
	unsigned int tmplen;
	const char* chrptr;

	struct shm_info* shmi;

	if((i->private_data = calloc(1, sizeof(struct shm_info)))==NULL) {
		ERR("failed to alloc shm_info");
		ret = EOUTOFMEM;
		goto out;
	}

	shmi = (struct shm_info*) i->private_data;
	
	/* config type_name */
	chrptr = (const char*) ubx_config_get_data_ptr(i, "type_name", &tmplen);
	if (chrptr == NULL || tmplen <= 0) {
	  ERR("%s: invalid or missing type name", i->name);
	  goto out_free_info;
	}
	shmi->data_type=ubx_type_get(i->ni, chrptr);
	if(shmi->data_type==NULL) {
	  ERR("%s: failed to lookup type %s", i->name, chrptr);
	  ret = EINVALID_CONFIG;
	  goto out_free_info;
	}
	
	/* config data_len */
	unsigned int array_len;
	array_len = *((uint32_t*) ubx_config_get_data_ptr(i, "array_len", &tmplen));
	array_len = (array_len == 0) ? 1 : array_len;
	shmi->data_len = array_len * shmi->data_type->size;

	/* retrive shm_name from config */
	chrptr = (char*) ubx_config_get_data_ptr(i, "shm_name", &tmplen);

	if(strcmp(chrptr, "")==0) {
		ERR("%s: shm_name is empty", i->name);
		goto out_free_info;
	}
	shmi->shm_name = strdup(chrptr);

	/* Map memory */
	shmi->map_descriptor = shm_open(shmi->shm_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	
	if (shmi->map_descriptor == -1) {
		ERR("%s: Error mmapping the memory %s", i->name, chrptr);
		goto out_free_info;
	}
	
	if ( ftruncate ( shmi->map_descriptor , shmi->data_len ) == -1 ) {
                close(shmi->map_descriptor);
                ERR("%s: Error ftruncating the memory %s", i->name, chrptr);
                goto out_free_info;
        }
	shmi->map = mmap(NULL, shmi->data_len, PROT_READ | PROT_WRITE, MAP_SHARED, shmi->map_descriptor, 0);
	if (shmi->map == MAP_FAILED){
	  close(shmi->map_descriptor);
	  ERR("%s: Error ftruncating the memory %s", i->name, chrptr);
	  goto out_free_info;
        }
	
	if ((shmi->mutex = sem_open(shmi->shm_name, O_CREAT, 0644, 1)) == SEM_FAILED) {
 	  close(shmi->map_descriptor);
          ERR("%s: Error creating the semphore %s", i->name, chrptr);
          goto out_free_info;
	}
	
	ret=0;
	goto out;

 out_free_info:
	free(shmi->shm_name);
	free(shmi);
 out:
	return ret;

}

static void shm_cleanup(ubx_block_t *i)
{
	int ret;
	
	struct shm_info* shmi = (struct shm_info*) i->private_data;

	if ( sem_destroy( shmi->mutex ) == -1 ) {
	  ERR("%s: Error destroing semaphore", i->name);
	}
	
	ret=munmap(shmi->map,shmi->data_len);
	if ( ret == -1) {
	  ERR("%s: Error un-mmapping the file", i->name);
	}
	free(shmi->shm_name);
	close(shmi->map_descriptor);
	free(shmi);

}

static int shm_read(ubx_block_t *i, ubx_data_t* data)
{
	int ret=0;
	struct shm_info* shmi;
	shmi = (struct shm_info*) i->private_data;
	
	if(shmi->data_type != data->type) {
		ERR("%s: invalid message type %s", i->name, data->type->name);
		goto out;
	}

	/* we let shm_send catch too large messages */
	sem_wait(shmi->mutex);
	memcpy(  data->data , shmi->map , shmi->data_len );
	sem_post(shmi->mutex);
out:
	return ret;
}

static void shm_write(ubx_block_t *i, ubx_data_t* data)
{
	int ret=0;
	struct shm_info* shmi;

	shmi = (struct shm_info*) i->private_data;
	if(shmi->data_type != data->type) {
		ERR("%s: invalid message type %s", i->name, data->type->name);
		goto out;
	}
	sem_wait(shmi->mutex);
	memcpy(  shmi->map , data->data , shmi->data_len );
	sem_post(shmi->mutex);

	if (ret<0){
		ERR("%s: Error copying data", i->name);
		goto out;
	}

 out:
	return;
}

/* put everything together */
ubx_block_t shm_comp = {
	.name = "control/shm",
	.type = BLOCK_TYPE_INTERACTION,
	.meta_data = shm_meta,
	.configs = shm_config,

	/* ops */
	.init = shm_init,
	.cleanup = shm_cleanup,
	.read=shm_read,
	.write=shm_write,

};

static int shm_mod_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &shm_comp);
}

static void shm_mod_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, "control/shm");
}

UBX_MODULE_INIT(shm_mod_init)
UBX_MODULE_CLEANUP(shm_mod_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)
