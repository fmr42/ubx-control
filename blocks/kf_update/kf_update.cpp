/*
 * Implementation of the update equations of kalman filter
 */

#define BLOCK_NAME "control/kf_update"

/* Disable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <armadillo>
#include <ubx/ubx.h>
using namespace std;
using namespace arma;

#ifdef __cplusplus
  extern "C" {
#endif

#include "../../types/control_types/State.h"
#include "../../types/control_types/SensorData.h"

char block_meta[] =
	"{ doc='Kalman filter - update equations',"
	"  real-time=true,"
	"}";

ubx_port_t block_ports[] = {
	{ .name = "sensor_in"   , .in_type_name  = "struct SensorData"	},
	{ .name = "state_in"    , .in_type_name  = "struct State"	},
	{ .name = "state_out"   , .out_type_name = "struct State"	},
	{ NULL }
};

ubx_config_t block_config[] = {
	{ .name="filepath_H"	, .type_name = "char"		, .doc="TODO" }, //TODO doc
	{ NULL }
};

/*										======== BLOCK INFO ======== */
struct block_info {
	mat R;
	mat H;
	vec Z;
	vec X_new;
	vec X_old;
	mat P_new;
	mat P_old;
	mat K;

	struct SensorData	ubx_sns_in	;
	struct State		ubx_stt_in	;
	struct State		ubx_stt_out	;

	/* Cached pointers to in and out ports */
	ubx_port_t*	ubx_p_sns_in  ;
	ubx_port_t*	ubx_p_stt_in   ;
	ubx_port_t*	ubx_p_stt_out  ;
};
/*										======== R/W FUNCS ======== */
// convenience functions
def_read_fun	( read_sns_in	, struct SensorData	);
def_read_fun	( read_stt_in	, struct State		);
def_write_fun	( write_stt_out	, struct State		);
/*										======== INIT ========= */
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
 out:
	return ret;
}
/* 										======== CLEANUP ======== */
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}
/* 										======== START ======== */
static int block_start(ubx_block_t *b)
{
	unsigned int  tmplen;
	char *chrptr;
	struct block_info* inf;

	inf=(struct block_info*) b->private_data;

	// Cache ports pointers
	inf->ubx_p_stt_in	= ubx_port_get ( b , "state_in"		);	
	inf->ubx_p_sns_in	= ubx_port_get ( b , "sensor_in"	);	
	inf->ubx_p_stt_out	= ubx_port_get ( b , "state_out"	);
	// Acquire mats from files specified in config
	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_H", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_H is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->H).load(chrptr)  ;

	//TODO perform checks on matrices size

	return 0;
}
/*										======== STEP ======== */
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	
	
	//TODO perform checks on mat and vec sizes
	// Read state. TODO maybe it is possible to avoid coping data
	read_stt_in ( inf->ubx_p_stt_in , &(inf->ubx_stt_in) );
	inf->X_old = vec ( (inf->ubx_stt_in).data	, (inf->ubx_stt_in).n_elem , false , true); 
	inf->P_old = mat ( (inf->ubx_stt_in).covariance	, (inf->ubx_stt_in).n_elem , (inf->ubx_stt_in).n_elem , false , true);
	// Read from sensor. TODO maybe it is possible to avoid coping data
	read_sns_in ( inf->ubx_p_sns_in , &(inf->ubx_sns_in) );
	inf->Z = vec ( (inf->ubx_sns_in).sns_data	, (inf->ubx_sns_in).n_elem , false , true); 
	inf->R = mat ( (inf->ubx_sns_in).sns_data_cov	, (inf->ubx_sns_in).n_elem , (inf->ubx_sns_in).n_elem , false , true);

	// Compute Kalman gain
	inf->K = inf->P_old * (inf->H).t()  * inv( inf->H * inf->P_old * (inf->H).t() + inf->R )  ;
	// Update state
	inf->X_new = inf->X_old + inf->K * ( inf->Z - inf->H * inf->X_old);
	// Update state cov mat
	inf->P_new = ( eye((inf->X_old).n_elem,(inf->X_old).n_elem) - inf->K * inf->H ) * inf->P_old;

	memcpy ( (inf->ubx_stt_out).data	, (inf->X_new).memptr() , (inf->X_new).n_elem*sizeof(double) );
	memcpy ( (inf->ubx_stt_out).covariance	, (inf->P_new).memptr() , (inf->P_new).n_cols*(inf->P_new).n_rows*sizeof(double) );
	(inf->ubx_stt_out).n_elem = (inf->X_new).n_elem;
	write_stt_out ( inf->ubx_p_stt_out , &(inf->ubx_stt_out) );
}



ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,

	/* ops */
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
