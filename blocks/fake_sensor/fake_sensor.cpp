/*
 * Output constant data simulating a sensor.
 * Data is corrupted with gaussan noise with variance 1 multiplied
 * by a gain specified as configuration value.
 */

#define BLOCK_NAME "control/fake_sensor"

//#define DEBUG 1
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <armadillo>
#include <ubx/ubx.h>
using namespace std;
using namespace arma;

#ifdef __cplusplus
  extern "C" {
#endif

#include "../../types/control_types/SensorData.h"

char block_meta[] =
	"{ doc='Output fake constant sensor data',"
	"  real-time=false,"
	"}";

ubx_config_t block_config[] = {
	{ .name="noise_gain"	, .type_name = "double"	, .doc="TODO" }, //TODO doc
	{ .name="filepath_Z"	, .type_name = "char"	, .doc="TODO" }, //TODO doc
	{ NULL }
};

struct block_info {
	vec Z;
	vec Z_noisy;
	mat R;
	double noise_gain;
	struct SensorData ubx_sns_out;
};

static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}

 out:
	return ret;
}

static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}

static int block_start(ubx_block_t *b)
{
	unsigned int  tmplen;
	char *chrptr;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	inf->noise_gain = *(double*) ubx_config_get_data_ptr(b, "noise_gain", &tmplen);

	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_Z", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_Z is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->Z).load  ( chrptr )  ;

	arma_rng::set_seed_random();
	
	(inf->R) = (inf->noise_gain) * (inf->noise_gain) * eye<mat>( (inf->Z).n_elem , (inf->Z).n_elem );

	return 0;
}

static int block_read(ubx_block_t *b, ubx_data_t* data) {
	unsigned int len;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	//Generate data
	(inf->Z_noisy) = (inf->Z) + (inf->noise_gain) * randn<vec>((inf->Z).n_elem);

	

	
	memcpy ( (inf->ubx_sns_out).sns_data	  , (inf->Z_noisy).memptr() , (inf->Z).n_elem*sizeof(double)                 );
	memcpy ( (inf->ubx_sns_out).sns_data_cov  , (inf->R).memptr()       , (inf->R).n_rows*(inf->R).n_cols*sizeof(double) );
	(inf->ubx_sns_out).n_elem = (inf->Z).n_elem;

	// Write out data
	len = sizeof(struct SensorData);
	memcpy ( data->data , &(inf->ubx_sns_out) , len ) ;
	return len;

}

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_INTERACTION,
	.meta_data = block_meta,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.cleanup = block_cleanup,
	.read = block_read,
};

static int mod_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void mod_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

UBX_MODULE_INIT(mod_init)
UBX_MODULE_CLEANUP(mod_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)

#ifdef __cplusplus
}
#endif

