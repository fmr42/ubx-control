 /*
 * TODO
 */

#define BLOCK_NAME "copter/ctrl_copter"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <armadillo>
#include <ubx/ubx.h>
using namespace std;
using namespace arma;


#include "../../types/control_types/State.h"
#include "../../types/control_types/SetPoint.h"
#include "../../types/control_types/ControlAction.h"
#include "../../include/arma_quat_lib.cpp"
#include "../../include/common_functions.hpp"
#include "ctrl_functions.cpp"


/*========================   BLOCK METADATA   ====================================================*/
char block_meta[] =
	"{ doc='Controller for quadricopter'," //TODO
	"  real-time=true,"
	"}";
/*========================   BLOCK PORTS   =======================================================*/
ubx_port_t block_ports[] = {
	{ .name = "state"	, .in_type_name  = "struct State"		},
	{ .name = "setpoint"	, .in_type_name  = "struct SetPoint"		},
	{ .name = "ctrl_action"	, .out_type_name = "struct ControlAction"	},
	{ NULL }
};
/*====================   BLOCK CONFIG   ====================*/
ubx_config_t block_config[] = {
	{ .name="mass"  , .type_name = "double", .doc="" },
	{ .name="inertia_xx"  , .type_name = "double", .doc="" },
	{ .name="inertia_yy"  , .type_name = "double", .doc="" },
	{ .name="inertia_zz"  , .type_name = "double", .doc="" },

	{ .name="l1"  , .type_name = "double", .doc="" },
	{ .name="l2"  , .type_name = "double", .doc="" },
	{ .name="k1"  , .type_name = "double", .doc="" },
	{ .name="k2"  , .type_name = "double", .doc="" },
	{ .name="kp"  , .type_name = "double", .doc="" },
	{ .name="kd"  , .type_name = "double", .doc="" },

	{ .name="hysteresis_thr"  , .type_name = "double", .doc="" },

	{ .name="max_thrust"		, .type_name = "double", .doc="" },
	{ .name="max_torque_xx"		, .type_name = "double", .doc="" },
	{ .name="max_torque_yy"		, .type_name = "double", .doc="" },
	{ .name="max_torque_zz"		, .type_name = "double", .doc="" },

	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/*==========================================		BLOCK DATA		==========================================*/
struct block_info {
	// Constants to be configured
	double l1;
	double l2;
	double k1;
	double k2;

	double M;
	vec e3;
	mat J;
	double Kp;
	double Kd;
#define G 9.81
	// Cached pointers to in and out ports
	ubx_port_t*	p_stt	;
	ubx_port_t*	p_sp	;
	ubx_port_t*	p_ctrl	;

	// Cache memory for input and output data
	struct State		ubx_stt;
	struct SetPoint		ubx_sp;
	struct ControlAction	ubx_ctrl;
  
	// Copter State
	vec P0;
	vec P1;
	vec Q0;
	vec Q1;
	vec W0;
	// Copter setpoint
	vec Pr0;
	vec Pr1;
	vec Pr2;
	vec Qr0;
	vec Qr1;
	vec Qr2;
	// 

	vec Vcr0;


	
	vec k0;
	vec Vc0;

	vec Qc_prime;
	vec Qc0 ;
	vec Qc1 ; 
	vec Qc2 ;
	vec Wc0 ;
	vec Wc1 ;

	vec Qc0_old ;
	vec Qc1_old ; 

	vec Re  ;
	vec Wce0;
	vec We0 ;
	vec Qe0 ;
	

	int h;
	double hysteresis_thr;

	double Uf ;	
	vec UtFF  ;
	vec UtFB  ;
	vec Ut    ;

	double	Uf_max	;	
	vec	Ut_max	;

	Timer tmr;

};

// convenience functions for I/O
def_read_fun  ( read_stt	, struct State		);
def_read_fun  ( read_sp	, struct SetPoint	);
def_write_fun ( write_ctrl_act	, struct ControlAction	);

/*==========================================		BLOCK INIT		==========================================*/
// This function allocate the memory required by this block
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
	
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	
	// Cache ports pointers
	inf->p_stt	= ubx_port_get ( b , "state"  );	
	inf->p_sp	= ubx_port_get ( b , "setpoint" );
	inf->p_ctrl	= ubx_port_get ( b , "ctrl_action" );

 out:
	return ret;
}

/*==========================================		BLOCK CLEANUP		==========================================*/
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}

/*==========================================		BLOCK START		==========================================*/
// This function read the block config
static int block_start(ubx_block_t *b)
{
	unsigned int  len;
	//char *chrptr;

	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	inf->e3 = zeros<vec>(3); inf->e3(2) = 1;

	//TODO read config
	inf->M = *((double*) ubx_config_get_data_ptr(b, "mass"  , &len));
	inf->J = eye(3,3);
	inf->J(0,0) = *((double*) ubx_config_get_data_ptr(b, "inertia_xx"  , &len));
	inf->J(1,1) = *((double*) ubx_config_get_data_ptr(b, "inertia_yy"  , &len));
	inf->J(2,2) = *((double*) ubx_config_get_data_ptr(b, "inertia_zz"  , &len));


	inf->hysteresis_thr = *((double*) ubx_config_get_data_ptr(b, "hysteresis_thr"  , &len));

	inf->Kp = *((double*) ubx_config_get_data_ptr(b, "kp"  , &len));
	inf->Kd = *((double*) ubx_config_get_data_ptr(b, "kd"  , &len));
	inf->k1 = *((double*) ubx_config_get_data_ptr(b, "k1"  , &len));
	inf->k2 = *((double*) ubx_config_get_data_ptr(b, "k2"  , &len));
	inf->l1 = *((double*) ubx_config_get_data_ptr(b, "l1"  , &len));
	inf->l2 = *((double*) ubx_config_get_data_ptr(b, "l2"  , &len));

	inf->Uf_max = *((double*) ubx_config_get_data_ptr(b, "max_thrust"  , &len));
	inf->Ut_max = zeros<vec>(3);
	inf->Ut_max(0) = *((double*) ubx_config_get_data_ptr(b, "max_torque_xx"  , &len));
	inf->Ut_max(1) = *((double*) ubx_config_get_data_ptr(b, "max_torque_yy"  , &len));
	inf->Ut_max(2) = *((double*) ubx_config_get_data_ptr(b, "max_torque_zz"  , &len));

	// Copter State
	inf->P0 = zeros<vec>(3);
	inf->P1 = zeros<vec>(3);
	inf->Q0 = zeros<vec>(4);
	inf->Q1 = zeros<vec>(4);
	inf->W0 = zeros<vec>(4);
	// Copter setpoint
	inf->Pr0 = zeros<vec>(3);;
	inf->Pr1 = zeros<vec>(3);;
	inf->Pr2 = zeros<vec>(3);;
	inf->Qr0 = zeros<vec>(4);
	inf->Qr1 = zeros<vec>(4);
	inf->Qr2 = zeros<vec>(4);
	// 
#define G 9.81
	inf->Vcr0 = zeros<vec>(3);
	
	inf->k0 = zeros<vec>(3);
	inf->Vc0 = zeros<vec>(3);

	inf->Qc_prime = zeros<vec>(4);
	inf->Qc0 = zeros<vec>(4);
	inf->Qc1 = zeros<vec>(4);
	inf->Qc2 = zeros<vec>(4);
	inf->Wc0 = zeros<vec>(4);
	inf->Wc1 = zeros<vec>(4);

	inf->Qc0_old = zeros<vec>(4);	inf->Qc0_old(0)=1 ; 
	inf->Qc1_old = zeros<vec>(4);

	inf->Re   = eye<mat>(3,3);
	inf->Wce0 = zeros<vec>(4);
	inf->We0 = zeros<vec>(4);
	inf->Qe0 = zeros<vec>(4);
	

	inf->h=1;

	inf->Uf=0 ;	
	inf->UtFF = zeros<vec>(3);
	inf->UtFB = zeros<vec>(3);
	inf->Ut   = zeros<vec>(3);

	(inf->tmr).reset();

	// Init control action
	(inf->ubx_ctrl).data[0] = (inf->Uf)	;
	(inf->ubx_ctrl).data[1] = (inf->Ut)(0)	;
	(inf->ubx_ctrl).data[2] = (inf->Ut)(1)	;
	(inf->ubx_ctrl).data[3] = (inf->Ut)(2)	;
	(inf->ubx_ctrl).n_elem	= 4		;

	write_ctrl_act ( inf->p_ctrl , &(inf->ubx_ctrl) );

	return 0;
}

/*==========================================		BLOCK STEP		==========================================*/
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;


	// Read the state
	// TODO(opt) speedup the following step: it should be possible
	// to avoid data copying
	read_stt ( inf->p_stt , &(inf->ubx_stt) );

	if ( (inf->ubx_stt).n_elem != 21 ) {
	  ERR("%s: invalid copter state", b->name ) ;
	  // TODO take a decision in case of error
	  return ;
	}

	for ( int i = 0 ; i < 3 ; i++ ) {
	  (inf->P0)( i ) = ( inf->ubx_stt ).data [ i   ];
	  (inf->P1)( i ) = ( inf->ubx_stt ).data [ i+3 ];
//	  (inf->P2)( i ) = ( inf->ubx_stt ).data [ i+6 ];	// Not necessary here
	}
	for ( int i = 0 ; i < 4 ; i++ ) {
	  (inf->Q0)( i ) = ( inf->ubx_stt ).data[ i+9  ];
	  (inf->Q1)( i ) = ( inf->ubx_stt ).data[ i+13 ];
//	  (inf->Q2)( i ) = ( inf->ubx_stt ).data[ i+17 ];	// Not necessary here
	}


	// Import setpoint
	read_sp ( inf->p_sp , &(inf->ubx_sp) );
	if ( (inf->ubx_sp).n_elem != 21 ) {
	  ERR("%s: invalid copter setpoint", b->name ) ;
	  // TODO take a decision in case of error
	  return ;
	}
	for ( int i = 0 ; i < 3 ; i++ ) {
	  (inf->Pr0)(i) = (inf->ubx_sp).data[i];
	  (inf->Pr1)(i) = (inf->ubx_sp).data[i+3];
	  (inf->Pr2)(i) = (inf->ubx_sp).data[i+6];
	}
	for ( int i = 0 ; i < 4 ; i++ ) {
	  (inf->Qr0)(i) = (inf->ubx_sp).data[i+9];
	  (inf->Qr1)(i) = (inf->ubx_sp).data[i+13];
	  (inf->Qr2)(i) = (inf->ubx_sp).data[i+17];
	}

#ifndef UBX_NO_DEBUG
cout << "======== " << b->name << " debug dump ========" << endl	;
cout << "P0 = " << inf->P0.t() ;
cout << "P1 = " << inf->P1.t() ;
cout << "Q0 = " << inf->Q0.t() ;
cout << "Q1 = " << inf->Q1.t() ;
cout << "Pr0 = " << inf->Pr0.t() ;
cout << "Pr1 = " << inf->Pr1.t() ;
cout << "Qr0 = " << inf->Qr0.t() ;
cout << "Qr1 = " << inf->Qr1.t() ;
#endif

	// Computer thrust reference (4)
	// TODO Vcr0 should be saturated
	inf->Vcr0 = inf->M * ( G * inf->e3 - inf->Pr2 );
	
	// Compute trusth feedback(12)
	inf->k0 = inf->l2 * sat ( ( inf->k2/inf->l2 ) * ( ( inf->P1 - inf->Pr1 ) + inf->l1 * sat ( (inf->k1/inf->l1) * ( inf->P0 - inf->Pr0 ) ) ) ) ;
//cout << "ctrl k0 " << inf->k0.t() ;
	// Compute control thrust
	inf->Vc0 = inf->Vcr0 + inf->k0 ;
	// Compute thrust
	inf->Uf = norm ( inf->Vc0 , 2 ) ;

	// Compute control orientation
	mat Rc0 = zeros<mat>(3,3);
	mat Rr0 = arma_rodriguez(inf->Qr0);

	Rc0.col(2) = normalise ( inf->Vc0 );
	Rc0.col(0) = normalise ( cross ( Rr0.col(1) , Rc0.col(2)  ) ) ;
	Rc0.col(1) = normalise ( cross ( Rc0.col(2) , Rc0.col(0)  ) ) ;
	
	inf->Qc0 = arma_mat_to_q(Rc0);

	inf->Qc1 = inf->Qc0 - inf->Qc0_old ;
	inf->Qc2 = inf->Qc1 - inf->Qc1_old ;
	
	(inf->Wc0).subvec(1,3) = arma_quat_d_to_vel( inf->Qc0 , inf->Qc1 );
	//(inf->Wc1).subvec(1,3) = arma_quat_d_to_acc( inf->Qc0 , inf->Qc1 , inf->Qc2 );

	// Compute error coordinates
	inf->Qe0  = arma_quat_hamilton( arma_quat_inv(inf->Qc0) , inf->Q0 );
	inf->W0.subvec(1,3) = arma_quat_d_to_vel (inf->Q0 , inf->Q1 );
	mat Re = arma_rodriguez(inf->Qe0);
	(inf->Wce0).subvec(1,3) = Re.t() * (inf->Wc0).subvec(1,3);
	inf->We0  = inf->W0 - inf->Wce0 ;

	// Update hysteresis
	if ( inf->h * inf->Qe0(0) <= inf->hysteresis_thr ) {
	  inf->h = copysign ( 1 , inf->Qe0(0) ) ; // TODO h=-h should be the same and more efficent
	}

	// Compute control action
	inf->UtFF = inf->J * ( Re.t() * (inf->Wc1).subvec(1,3) ) - cross( inf->J * (inf->Wce0).subvec(1,3) , (inf->Wce0).subvec(1,3) ) ;
	inf->UtFB = - inf->Kp * inf->h * inf->Qe0.subvec(1,3) - inf->Kd * (inf->We0).subvec(1,3) ;
	inf->Ut = inf->UtFB + inf->UtFF ;

	vec Ut_sat = zeros<vec>(3); //TODO move this to block info for better performances
	double Uf_sat;

#ifndef UBX_NO_DEBUG
  cout << "Vcr0 = " << inf->Vcr0.t();
  cout << "k0 = " << inf->k0.t();
  cout << "Vc0 = " << inf->Vc0.t();
  cout << "Uf = " << inf->Uf << endl;
  cout << "Rr0 = " << endl << Rr0.t();
  cout << "Rc0 = " << endl << Rc0.t();
  cout << "Qc0 = " << inf->Qc0.t();
  cout << "Wc0 = " << inf->Wc0.t();
  cout << "Wc1 = " << inf->Wc1.t();

  cout << "Qe0 = " << inf->Qe0.t();
  cout << "W0 = " << inf->W0.t() ;
  cout << "We0 = " << inf->We0.t() ;
  cout << "h = " << inf->h ;
cout << "ctrl UtFF " << inf->UtFF.t() ;
cout << "ctrl UtFB " << inf->UtFB.t() ;
  cout << "Ut = " << inf->Ut.t() ;
#endif
/*	TODO This is not the correct way to saturate */

	if ( inf->Uf >= 0 ) {
	  Uf_sat = min ( +inf->Uf_max , inf->Uf ) ;
	}else{
	  Uf_sat = max ( -inf->Uf_max , inf->Uf ) ;
	}
	
	for (int i = 0 ; i<3 ; i++ ){
	  if ( inf->Ut(i) >= 0 ) {
	    Ut_sat(i) = min ( +inf->Ut_max(i) , inf->Ut(i) ) ;
	  }else{
	    Ut_sat(i) = max ( -inf->Ut_max(i) , inf->Ut(i) ) ;
	  }
	}

	// Output control action
	(inf->ubx_ctrl).data[0] = (Uf_sat) ;
	(inf->ubx_ctrl).data[1] = (Ut_sat)(0) ;
	(inf->ubx_ctrl).data[2] = (Ut_sat)(1) ;
	(inf->ubx_ctrl).data[3] = (Ut_sat)(2) ;
	(inf->ubx_ctrl).n_elem=4;
	write_ctrl_act ( inf->p_ctrl , &(inf->ubx_ctrl) );
	//TODO set metadata


	// Store some values to compute derivatives next loop
	inf->Qc0_old = inf->Qc0 ;
	inf->Qc1_old = inf->Qc1 ;
	return;
}

/*==========================================		BUILD BLOCK		==========================================*/

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
