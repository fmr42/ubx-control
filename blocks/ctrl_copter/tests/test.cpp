
#include <armadillo>
#include <iostream>
#include "../ctrl_functions.cpp"

using namespace std;
using namespace arma;


int main () {

	mat v = zeros(23,2);
	v.col(0)=linspace(-1.1,1.1,23);
	v.col(1) = sat(v.col(0));
	cout << "	In	out"<< endl << v ;

	return 0;
}
