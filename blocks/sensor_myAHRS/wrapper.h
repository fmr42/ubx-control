#ifndef __MYWRAPPER_H
  #define __MYWRAPPER_H

  #ifdef __cplusplus
    extern "C" {
  #endif

    typedef struct wrp_sensor wrp_sensor;
    wrp_sensor*	wrp_new_sensor	  ( )					;
    int wrp_sensor_start ( wrp_sensor *v , char *dev_path)		;
    int	wrp_sensor_get_quaternion ( wrp_sensor* v , double *ptr )	;
    int	wrp_sensor_stop		  ( wrp_sensor* v )			;

  #ifdef __cplusplus
    }
  #endif
#endif
