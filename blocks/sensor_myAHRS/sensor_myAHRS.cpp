
#define BLOCK_NAME "control/sensor_myAHRS"

//#define ARMA_NO_DEBUG

#include <iostream>
#include <armadillo>


#include <ubx/ubx.h>
using namespace std;
using namespace arma;

#include "wrapper.h"

#ifdef __cplusplus
  extern "C" {
#endif

#include "../../types/control_types/SensorData.h"

char block_meta[] =
	"{ doc='Output fake constant sensor data',"
	"  real-time=false,"
	"}";

ubx_config_t block_config[] = {
	{ .name="device_path"		, .type_name = "char"	, .doc="TODO" }, //TODO doc
	{ .name="quaternion_cov"	, .type_name = "double"	, .doc="TODO" }, //TODO doc

	{ NULL }
};

struct block_info {
	double		quaternion_cov;
	unsigned int	last_sample_count;
	char		*device_path;
	vec Z;
	mat R;
	struct SensorData ubx_sns_out;
	struct wrp_sensor *sns;
};

static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}

 out:
	return ret;
}

static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}

static int block_start(ubx_block_t *b)
{

	unsigned int  tmplen;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	inf->quaternion_cov = *(double*) ubx_config_get_data_ptr(b, "quaternion_cov", &tmplen);


	inf->device_path    =  (char*) ubx_config_get_data_ptr(b, "device_path", &tmplen);
	if(strcmp(inf->device_path, "")==0) {
		ERR("%s: device filepath is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	inf->sns = wrp_new_sensor();
	
	int ret = wrp_sensor_start(inf->sns , inf->device_path);
	if ( ret  != 0 ) {
		ERR("%s: wrp_sensor_start() on %s returned %d", b->name, inf->device_path , ret);
		return -2;
	}

	inf->last_sample_count = 0;

	inf->Z = zeros<vec>(4);
	inf->R = eye<mat>(4,4) * inf->quaternion_cov; //TODO this is an approximation
	return 0;
}

static int block_read(ubx_block_t *b, ubx_data_t* data) {
	unsigned int len;
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	
	inf->last_sample_count = wrp_sensor_get_quaternion( inf->sns , (inf->ubx_sns_out).sns_data );

	memcpy ( (inf->ubx_sns_out).sns_data_cov  , (inf->R).memptr() , (inf->R).n_rows*(inf->R).n_cols*sizeof(double) );
cout <<  (inf->ubx_sns_out).sns_data[0] << " " <<  (inf->ubx_sns_out).sns_data[1] << " " <<  (inf->ubx_sns_out).sns_data[2] << " " <<  (inf->ubx_sns_out).sns_data[4] << endl;
	(inf->ubx_sns_out).n_elem = (inf->Z).n_elem;

	// Write out data
	len = sizeof(struct SensorData);
	memcpy ( data->data , &(inf->ubx_sns_out) , len ) ;
	return len;

}

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_INTERACTION,
	.meta_data = block_meta,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.cleanup = block_cleanup,
	.read = block_read,
};

static int mod_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void mod_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

UBX_MODULE_INIT(mod_init)
UBX_MODULE_CLEANUP(mod_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)

#ifdef __cplusplus
}
#endif

