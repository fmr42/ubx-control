/*
 * Implementation of the prediction equations of a simple kalman filter
 */

#define BLOCK_NAME "control/kalman_predict"

/* Enable verbose output */
#define UBX_NO_DEBUG
/* Disable error checks by armadillo library. */
//#define ARMA_NO_DEBUG

#include <iostream>
#include <ctime>
#include <armadillo>
#include <ubx/ubx.h>
using namespace std;
using namespace arma;


#include "../../types/control_types/State.h"
#include "../../types/control_types/ControlAction.h"
#include "../../types/control_types/LinearSystem.h"

char block_meta[] =
	"{ doc='Kalman filter - prediction equations',"
	"  real-time=true,"
	"}";

ubx_port_t block_ports[] = {
	{ .name = "state_in"	, .in_type_name  = "struct State" },
	{ .name = "control_in"	, .in_type_name  = "struct ControlAction" },
	{ .name = "state_out"	, .out_type_name = "struct State" },
	{ .name = "sys_in"	, .in_type_name = "struct LinearSystem" },

	{ NULL }
};

ubx_config_t block_config[] = {
	{ .name="filepath_X0"	, .type_name = "char"		, .doc="TODO" }, //TODO doc
	{ .name="filepath_P0"	, .type_name = "char"		, .doc="TODO" }, //TODO doc
	{ .name="filepath_Q"	, .type_name = "char"		, .doc="TODO" }, //TODO doc
	{ .name="do_init"	, .type_name = "int"		, .doc="TODO" }, //TODO doc
	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/* ================================   BLOCK INFO   ================================ */
struct block_info {
  /* Cached pointers to in and out ports */
  mat	A	;
  mat	B	;
  mat	Q	;
  vec	X	;
  vec	Xnew	;
  vec	U	;
  mat	P	;
  mat	Pnew	;

  ubx_port_t*	ubx_p_stt_in	;
  ubx_port_t*	ubx_p_ctrl_in	;
  ubx_port_t*	ubx_p_stt_out	;
  ubx_port_t*	ubx_p_sys_in	;

  struct State		ubx_stt_in	;
  struct ControlAction	ubx_ctrl_in	;
  struct State		ubx_stt_out	;
  struct LinearSystem		ubx_sys_in	;
};
/* ================================   R/W FUNCS   ================================ */
def_read_fun  ( read_state_in	, struct State		);
def_read_fun  ( read_ctrl_in	, struct ControlAction	);
def_write_fun ( write_state_out	, struct State		);
def_read_fun  ( read_sys_in	, struct LinearSystem	);
/* ================================   INIT   ================================ */
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
  out:
	return ret;
}
/* ================================   CLEANUP   ================================ */
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}
/* ================================   START   ================================ */
static int block_start(ubx_block_t *b)
{
	unsigned int  tmplen;
	/* 0 -> Do not init state
	   1 -> Do     init state */
	int do_init;

	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	char *chrptr;

	// Cache ports pointers
	inf->ubx_p_stt_in	= ubx_port_get ( b , "state_in"		);
	inf->ubx_p_ctrl_in	= ubx_port_get ( b , "control_in"	);
	inf->ubx_p_stt_out	= ubx_port_get ( b , "state_out"	);
	inf->ubx_p_sys_in	= ubx_port_get ( b , "sys_in"	);

	// Read config
	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_X0", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_X0 is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->X).load(chrptr)  ;
	inf->Xnew = inf->X     ;

	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_Q", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_Q is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->Q).load(chrptr);

	chrptr = (char*) ubx_config_get_data_ptr(b, "filepath_P0", &tmplen);
	if(strcmp(chrptr, "")==0) {
		ERR("%s: filepath_P0 is empty", b->name);
		block_cleanup(b);
		return -1;
	}
	(inf->P).load(chrptr);
	(inf->Pnew)=(inf->P) ;
	
	int *ptr = (int*) ubx_config_get_data_ptr(b, "do_init", &tmplen);
	do_init = *ptr;
	if ( do_init!=0 ) {
	  cout << "Init state..." << endl ;
	  // TODO Check matrices sizes and consistency
	  memcpy ( (inf->ubx_stt_out).data		, (inf->Xnew).memptr() , (inf->Xnew).n_elem*sizeof(double) );
	  memcpy ( (inf->ubx_stt_out).covariance	, (inf->Pnew).memptr() , (inf->Pnew).n_cols*(inf->Pnew).n_rows*sizeof(double) );
	           (inf->ubx_stt_out).n_elem = (inf->Xnew).n_elem;
	  write_state_out ( inf->ubx_p_stt_out , &(inf->ubx_stt_out) );
	}else{
	  cout << "State _NOT_ initialized!" << endl ;
	}
	
	return 0;
	
}

static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	
	// Read state TODO maybe it is possible to avoid coping data
	read_state_in ( inf->ubx_p_stt_in , &(inf->ubx_stt_in) );
	inf->X = vec ( (inf->ubx_stt_in).data		, (inf->ubx_stt_in).n_elem , false , true); 
	inf->P = mat ( (inf->ubx_stt_in).covariance	, (inf->ubx_stt_in).n_elem , (inf->ubx_stt_in).n_elem , false , true); 
	// Control input TODO maybe it is possible to avoid coping data
	read_ctrl_in ( inf->ubx_p_ctrl_in , &(inf->ubx_ctrl_in) );
	inf->U = vec ( (inf->ubx_ctrl_in).data , (inf->ubx_ctrl_in).n_elem , false , true); 
	// Read system TODO maybe it is possible to avoid coping data
	read_sys_in ( inf->ubx_p_sys_in , &(inf->ubx_sys_in) );
	inf->A = mat ( (inf->ubx_sys_in).A.data	, (inf->ubx_sys_in).A.n_rows , (inf->ubx_sys_in).A.n_cols , false , true);
	inf->B = mat ( (inf->ubx_sys_in).B.data	, (inf->ubx_sys_in).B.n_rows , (inf->ubx_sys_in).B.n_cols , false , true);

	// Compute new state
	( inf->Xnew ) = ( inf->A ) * ( inf->X ) + (inf->B) * (inf->U)		;
	( inf->Pnew ) = ( inf->A ) * ( inf->P ) * ( (inf->A).t() ) + (inf->Q)	;

	memcpy ( (inf->ubx_stt_out).data	, (inf->Xnew).memptr() , (inf->Xnew).n_elem*sizeof(double) );
	memcpy ( (inf->ubx_stt_out).covariance	, (inf->Pnew).memptr() , (inf->Pnew).n_cols*(inf->Pnew).n_rows*sizeof(double) );
	(inf->ubx_stt_out).n_elem = (inf->Xnew).n_elem;
	write_state_out ( inf->ubx_p_stt_out , &(inf->ubx_stt_out) );
}



ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif
