#define BLOCK_NAME "copter/copter_graphic_dump"

#include <iostream>
#include <ctime>
#include <ubx/ubx.h>
#include <cmath>
#include <sstream>
#include <iomanip> 


#include "../../types/control_types/SetPoint.h"
#include "../../types/control_types/State.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;


/*========================   BLOCK METADATA   ====================================================*/
char block_meta[] =
	"{ doc=''," //TODO
	"  real-time=true,"
	"}";
/*========================   BLOCK PORTS   =======================================================*/
ubx_port_t block_ports[] = {
	{ .name = "state"	, .in_type_name  = "struct State"		},
	{ .name = "setpoint"	, .in_type_name  = "struct SetPoint"		},
	{ NULL }
};
/*====================   BLOCK CONFIG   ====================*/
ubx_config_t block_config[] = {
	{ .name="zoom"		, .type_name = "double", .doc="" },
	{ .name="max_q"		, .type_name = "double", .doc="" },

	{ NULL }
};

// Do not use overloading!! This is required to mantain copatibility with C
#ifdef __cplusplus
  extern "C" {
#endif
/*==========================================		BLOCK DATA		==========================================*/
struct block_info {

	// Cached pointers to in and out ports
	ubx_port_t*	p_stt	;
	ubx_port_t*	p_sp	;
	double zoom;
	// Cache memory for input and output data
	struct State		ubx_stt;
	struct SetPoint		ubx_sp;
	Mat *image;
	Mat *white_image;	//used for trasparency
	double max_q_square;

};

// convenience functions for I/O
def_read_fun  ( read_stt	, struct State		);
def_read_fun  ( read_sp		, struct SetPoint	);

/*==========================================		BLOCK INIT		==========================================*/
// This function allocate the memory required by this block
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
	
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	
	// Cache ports pointers
	inf->p_stt	= ubx_port_get ( b , "state"  );	
	inf->p_sp	= ubx_port_get ( b , "setpoint" );


 out:
	return ret;
}

/*==========================================		BLOCK CLEANUP		==========================================*/
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}

/*==========================================		BLOCK START		==========================================*/
// This function read the block config
static int block_start(ubx_block_t *b)
{
//	unsigned int  len;
	//char *chrptr;

	struct block_info* inf;
	inf=(struct block_info*) b->private_data;
	unsigned int  len;
	inf->zoom = *((double*) ubx_config_get_data_ptr(b, "zoom", &len));
	if ( inf->zoom <= 0 ) {
		cout << "Zoom not valid. setting to 10" << endl;
		inf->zoom=10; 
	}
	inf->max_q_square = 0 ;
	inf->max_q_square = pow(*((double*) ubx_config_get_data_ptr(b, "max_q", &len)),2);

	inf->image = new Mat(501, 701, CV_8UC3);
	inf->white_image = new Mat(501, 701, CV_8UC3);
	(inf->image)->setTo(Scalar(255,255,255));
	(inf->white_image)->setTo(Scalar(255,255,255));

	return 0;
}

/*==========================================		BLOCK STEP		==========================================*/
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;


	read_stt	( inf->p_stt	, &(inf->ubx_stt)	);
	read_sp		( inf->p_sp	, &(inf->ubx_sp)	);

#define USE_TRASPARENCY
#ifdef USE_TRASPARENCY
	double old_image_intensity = 0.2;
	cv::addWeighted ( *(inf->image) , old_image_intensity, *(inf->white_image) , 1.0 - old_image_intensity , 0.0 , *(inf->image) ); 
#else
	(inf->image)->setTo(Scalar(255,255,255)); //no trasparency
#endif



	// Draw trajectory
double zoom = inf->zoom;


	line( *(inf->image), Point(351,0), Point(351,500),  Scalar( 0, 0, 0 ) );
	line( *(inf->image), Point(0,251), Point(700,251),  Scalar( 0, 0, 0 ) );


line( *(inf->image), Point(0,0), Point(700,0),  Scalar( 0, 0, 0 ) );
line( *(inf->image), Point(0,0), Point(0,500),  Scalar( 0, 0, 0 ) );
line( *(inf->image), Point(700,500), Point(0,500),  Scalar( 0, 0, 0 ) );
line( *(inf->image), Point(700,500), Point(700,0),  Scalar( 0, 0, 0 ) );



	double q_square = 0;
	for(int i=0 ; i<6 ; i++) {
	  q_square = q_square + pow((inf->ubx_stt.data[i] - inf->ubx_sp.data[i]),2)	;
	}
	Scalar txt_color;
	if ( q_square < inf->max_q_square ) {
	  txt_color = Scalar( 0, 255 , 0 );
	} else {
	  txt_color = Scalar( 0, 0 , 255 );
	}
//const char* c_str();
ostringstream message;
message.str(std::string()); // clean it
message << setprecision(2) << setfill('0') << setw(6) << fixed << std::showpos << "pos: x = " << inf->ubx_stt.data[0] << " / " << inf->ubx_sp.data[0] << " ; y = " << inf->ubx_stt.data[1] << " / " << inf->ubx_sp.data[1] << " ; z = " << inf->ubx_stt.data[2] << " / " << inf->ubx_sp.data[2] ;
putText(*(inf->image), message.str() , Point(5,18), FONT_HERSHEY_DUPLEX, 0.4, txt_color,1,8 );
message.str(std::string()); // clean it
message << setprecision(2) << setfill('0') << setw(6) << fixed << std::showpos << "vel: x = " << inf->ubx_stt.data[3] << " / " << inf->ubx_sp.data[3] << " ; y = " << inf->ubx_stt.data[4] << " / " << inf->ubx_sp.data[4] << " ; z = " << inf->ubx_stt.data[5] << " / " << inf->ubx_sp.data[5] ;
putText(*(inf->image), message.str() , Point(5,36), FONT_HERSHEY_DUPLEX, 0.4, txt_color,1,8 );
message.str(std::string()); // clean it
message << setprecision(2) << fixed << std::showpos << "q = " << sqrt(q_square) << " / " << sqrt(inf->max_q_square) ;
putText(*(inf->image), message.str() , Point(5,54), FONT_HERSHEY_DUPLEX, 0.4, txt_color,1,8 );



	// Draw set point
{
	// TODO write documentation for the following step!!!
	// Compute vector "heading" in the world reference frame
	double x1 = 1 - 2*pow(inf->ubx_sp.data[11],2) - 2*pow(inf->ubx_sp.data[12],2) ;
	double x2 = 2 * ( inf->ubx_sp.data[10]*inf->ubx_sp.data[11] + inf->ubx_sp.data[9]*inf->ubx_sp.data[12] ) ;
	double h1 = x1 / sqrt( x1*x1 + x2*x2 ) ;
	double h2 = x2 / sqrt( x1*x1 + x2*x2 ) ;
	// position set-point in OpenCV Reference frame
	Point trj_pt = Point ( 351 + ( inf->ubx_sp.data[1] * zoom) , 251 + ( -inf->ubx_sp.data[0] * zoom)  );
	// position set-point + heading in OpenCV Reference frame
	Point heading_pt = Point ( trj_pt.x + 10 * h2 , trj_pt.y - 10 * h1 );
	circle		( *(inf->image) , trj_pt , 5.0, Scalar( 0 , 255 , 0 ) , 2 , 8 );	//green
	line	( *(inf->image) , trj_pt , heading_pt,  Scalar( 0 , 255 , 0 ) , 2 , 8 );
}
	// Draw state
{
	// TODO write documentation for the following step!!!
	// Compute vector "heading" in the world reference frame
	double x1 = 1 - 2*pow(inf->ubx_stt.data[11],2) - 2*pow(inf->ubx_stt.data[12],2) ;
	double x2 = 2 * ( inf->ubx_stt.data[10]*inf->ubx_stt.data[11] + inf->ubx_stt.data[9]*inf->ubx_stt.data[12] ) ;
	double h1 = x1 / sqrt( x1*x1 + x2*x2 ) ;
	double h2 = x2 / sqrt( x1*x1 + x2*x2 ) ;
	// position set-point in OpenCV Reference frame
	Point stt_pt = Point ( 351 + ( inf->ubx_stt.data[1] * zoom) , 251 + ( -inf->ubx_stt.data[0] * zoom)  );
	// position set-point + heading in OpenCV Reference frame
	Point heading_pt = Point ( stt_pt.x + 10 * h2 , stt_pt.y - 10 * h1 );
	circle		( *(inf->image) , stt_pt , 5.0, Scalar( 0 , 0 , 255 ) , 2 , 8 );	//red
	line	( *(inf->image) , stt_pt , heading_pt,  Scalar( 0 , 0 , 255 ) , 2 , 8 );
}



	// show image and terminate
	imshow("image", *(inf->image));
	waitKey(50);
	return;
}

/*==========================================		BUILD BLOCK		==========================================*/

ubx_block_t block_comp = {
	.name = BLOCK_NAME,
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,
	.configs = block_config,
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

static int module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

static void module_cleanup(ubx_node_info_t *ni)
{
	ubx_block_unregister(ni, BLOCK_NAME);
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(module_init)
UBX_MODULE_CLEANUP(module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)


#ifdef __cplusplus
}
#endif



