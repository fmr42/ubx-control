
#include <armadillo>
#include <iostream>
#include "../../include/arma_quat_lib.cpp"

using namespace std;
using namespace arma;


int main () {

	{
	  cout << "Testing 'vec arma_quat_hamilton(const vec& a,const vec& b)'...\n" ;
	  cout << "   Result should be [1 0 0 0] trasposed\n" ;

	  vec q1(4);
	  vec q2(4);
	  vec q3(4);

	  q1(0) = + 0;
	  q1(1) = + 0;
	  q1(2) = + 1;
	  q1(3) = + 0;

	  q2(0) = + 0;
	  q2(1) = + 0;
	  q2(2) = - 1;
	  q2(3) = - 0;
	
	  q3 = arma_quat_hamilton ( q1 , q2 );
	  cout << q3 ;
	}

	{
	  cout << "\n=======================================\n" ;
	  cout << "Testing 'arma_quat_inv'...\n" ;
	  cout << "   Result should be [1 0 0 0] trasposed\n" ;

	  vec q1(4);
	  vec q2(4);
	  vec q3(4);

	  q1(0) = + 0;
	  q1(1) = + 0;
	  q1(2) = + 1;
	  q1(3) = + 0;

	  q2 = arma_quat_inv(q1) ;
	
	  q3 = arma_quat_hamilton ( q1 , q2 );
	  cout << q3 ;
	}

	{
	  cout << "\n=======================================\n" ;
	  cout << "Testing 'arma_quat_between_vecs'...\n" ;
	  cout << "TODO: implement special case a=-b\n";
	  cout << "   Result should be [0.7071 0 0.7071 0] trasposed\n" ;

	  vec a(3) ;
	  vec b(3) ;
	  vec q(4) ;

	  a(0) = 2 ;
	  a(1) = 0 ;
	  a(2) = 0 ;

	  b(0) =  0 ;
	  b(1) =  0 ;
	  b(2) = -3 ;

	  q = arma_quat_between_vecs(a,b);
	  cout << q << endl;
	}

	{
	  cout << "\n=======================================\n" ;
	  cout << "Testing 'arma_quat_d_to_vel'...\n" ;
	  cout << "   Result should be [0 1 0] trasposed\n" ;

	  vec q0 (4) ;
	  vec q1 (4) ;
	  vec w  (3) ;

	  q0(0) = 1 ;
	  q0(1) = 0 ;
	  q0(2) = 0 ;
	  q0(3) = 0 ;

	  q1(0) = 0 ;
	  q1(1) = 0 ;
	  q1(2) = 0.5 ;
	  q1(3) = 0 ;

	  w = arma_quat_d_to_vel(q0,q1);
	  cout << w << endl;
	}

	{
	  cout << "\n=======================================\n" ;
	  cout << "Testing 'arma_quat_d_to_acc'...\n" ;
	  cout << "   Result should be [0 20 0] trasposed\n" ;
//Suppose w = 1
	  vec q0 (4) ;
	  vec q1 (4) ;
	  vec q2 (4) ;
	  vec w1  (3) ;

	  q0(0) = 1 ;
	  q0(1) = 0 ;
	  q0(2) = 0 ;
	  q0(3) = 0 ;

	  q1(0) = 0 ;
	  q1(1) = 0 ;
	  q1(2) = 0.5 ;
	  q1(3) = 0 ;

	  q2(0) = -1/4 ;
	  q2(1) = 0 ;
	  q2(2) = 10 ;
	  q2(3) = 0 ;

	  w1 = arma_quat_d_to_acc(q0,q1,q2);
	  cout << w1 << endl;
	}

	{
	  cout << "\n=======================================\n" ;
	  cout << "Testing 'arma_quat_rot'...\n" ;
	  cout << "   Result should be [5 5 -5] trasposed\n" ;

	  vec q (4) ;
	  vec v (3) ;
	  q(0) = cos(datum::pi/4) ;
	  q(1) = 0 ;
	  q(2) = sin(datum::pi/4) ;
	  q(3) = 0 ;

	  v(0) = 5 ;
	  v(1) = 5 ;
	  v(2) = 5 ;
	  
	  v = arma_quat_rot(q,v);
	  cout << v << endl;
	}
	{
	  cout << "\n=======================================\n" ;
	  cout << "Testing 'arma_cross_mat'...\n" ;
	  cout << "   Result should be\n" ;
	  cout << "   |  0 -3  2 |\n" ;
	  cout << "   |  3  0 -1 |\n" ;
	  cout << "   | -2  1  0 |\n" ;

	  mat M ;
	  vec v (3) ;

	  v(0) = 1 ;
	  v(1) = 2 ;
	  v(2) = 3 ;

	  M = arma_cross_to_mat(v);
	  cout << M << endl;
	}
	{
	  cout << "\n=======================================\n" ;
	  cout << "Testing 'arma_rodriguez'...\n" ;
	  cout << "   Result should be\n" ;
	  cout << "   | -1  0  0 |\n" ;
	  cout << "   |  0  1  0 |\n" ;
	  cout << "   |  0  0 -1 |\n" ;

	  mat R ;
	  vec q (4) ;

	  q(0) =  0 ;
	  q(1) =  0 ;
	  q(2) = -1 ;
	  q(3) =  0 ;

	  R = arma_rodriguez(q);
	  cout << R << endl;
	}
	{
	cout << "\n=======================================\n" ;
	cout << "Testing 'arma_mat_to_q'...\n" ;
	cout << "   Result should be\n" ;
	cout << "   [ 0.7071 0  0.7071  0 ] transposed\n" ;

	mat R = zeros<mat>(3,3) ;
	R(0,2) =  1;
	R(1,1) =  1;
	R(2,0) = -1;

	vec q = arma_mat_to_q(R);

	cout << q << endl;
	}
	{
	cout << "\n=======================================\n" ;
	cout << "Testing 'arma_mat_to_q' for pi rotation...\n" ;
	cout << "   Result should be\n" ;
	cout << "   [ 0  0  1  0 ] transposed\n" ;

	mat R = zeros<mat>(3,3) ;
	R(0,0)= -1;
	R(1,1)=  1;
	R(2,2)= -1;

	vec q = arma_mat_to_q(R);

	cout << q << endl;
	}

	return 0;
}
