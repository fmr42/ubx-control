local bd = require("blockdiagram")

return bd.system {
	imports = {
		"types/stdtypes/stdtypes.so",
		"types/control/control_types.so",
		"blocks/ptrig/ptrig.so",
		"blocks/control/shm.so",
		"blocks/control/dump_ctrl.so",
		"blocks/control/dump_state.so",
		"blocks/control/kalman_predict.so",
		"blocks/control/kf_update.so",
		"blocks/control/fake_ctrl.so",
		"blocks/control/fake_sensor.so",
		"blocks/control/static_linear_system.so",

		--"blocks/control/state_dump.so",
	},

	blocks = {
		{ name = "trig_predict"	, type = "std_triggers/ptrig"		},
		{ name = "trig_update"	, type = "std_triggers/ptrig"		},
		-- i-blocks
		{ name = "action"	, type = "control/shm"			},
		{ name = "state"	, type = "control/shm"			},
		{ name = "sys"		, type = "control/shm"			},
		{ name = "sns"		, type = "control/fake_sensor"			},
		-- c-block
		{ name = "ctrl"		, type = "control/fake_ctrl"		},
		{ name = "kf_pre"	, type = "control/kalman_predict"	},
		{ name = "kf_upd"	, type = "control/kf_update"	},
		{ name = "linearizer"	, type = "control/static_linear_system"	},
		-- i-block for dump
		{ name = "action_dump"	, type = "control/dump_ctrl"		},
		{ name = "state_dump"	, type = "control/dump_state"		},
	},

	connections = {
		-- connect controller
		{ src="ctrl.control_out" 	, tgt="action"			},
		{ src="ctrl.control_out"	 , tgt="action_dump"			},
		-- connect kf predict
		{ src="kf_pre.state_out"	, tgt="state_dump"		},
		{ src="kf_pre.state_out"	, tgt="state"			},
		{ src="state"			, tgt="kf_pre.state_in"		},
		{ src="action"			, tgt="kf_pre.control_in"	},
		{ src="sys"			, tgt="kf_pre.sys_in"	},
		-- connect kf update
		{ src="kf_upd.state_out"	, tgt="state_dump"		},
		{ src="kf_upd.state_out"	, tgt="state"			},
		{ src="state"			, tgt="kf_upd.state_in"		},
		{ src="sns"			, tgt="kf_upd.sensor_in"	},

		-- connect system
		{ src="linearizer.sys_out"	, tgt="sys"	},
	},

	configurations = {
		{
			name="sys" ,
			config = {
				shm_name="/sys" ,
				type_name="struct LinearSystem",
			 },
		},
		{
			name="action" ,
			config = {
				shm_name="/action" ,
				type_name="struct ControlAction",
			 },
		},
		{
			name="state" ,
			config = {
				shm_name="/state" ,
				type_name="struct State",
			 },
		},
		{
			name="ctrl" ,
			config = {
				filepath_U="U",
			 },
		},
		{
			name="sns" ,
			config = {
				filepath_Z="Z",
				noise_gain=1,
			 },
		},
		{
			name="kf_upd" ,
			config = {
				filepath_H="H",
			 },
		},
		{
			name="kf_pre" ,
			config = {
				filepath_Q	= "Q"	,
				filepath_X0	= "X0"	,
				filepath_P0	= "P0"	,
				do_init		= 1	,
			},
		},
		{
			name="linearizer" ,
			config = {
				filepath_A	= "A"	,
				filepath_B	= "B"	,
			},
		},
		{
			name="trig_predict" ,
			config = {
				period = {sec=0, usec=100000 },
				trig_blocks={
					{ b="#kf_pre"	, num_steps=1, measure=0 },
				},
			},
		},
		{
			name="trig_update" ,
			config = {
				period = {sec=0, usec=100000 },
				trig_blocks={
					{ b="#kf_upd"	, num_steps=1, measure=0 },
				},
			},
		},
	},

}


