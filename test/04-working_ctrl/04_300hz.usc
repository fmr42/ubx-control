local bd = require("blockdiagram")

return bd.system {
	imports = {
		"types/stdtypes/stdtypes.so",
		"types/control/control_types.so",
		"blocks/ptrig/ptrig.so",
		"blocks/control/shm.so",
		"blocks/control/dump_ctrl.so",
		"blocks/copter/ctrl_copter.so",
		"blocks/copter/prim_trj_gen.so",
		"blocks/copter/kf_predict_copter.so",
		"blocks/control/dump_state.so",
		"blocks/copter/copter_graphic_dump.so",
		"blocks/control/dump_time_stat.so",

	},

	blocks = {
		{ name = "ptrig1"	, type = "std_triggers/ptrig"			},
		{ name = "ptrig_graphic", type = "std_triggers/ptrig"			},
		{ name = "ctrl_action"	, type = "control/shm"				},
		{ name = "state"	, type = "control/shm"				},
		{ name = "setpoint"	, type = "control/shm"				},
		{ name = "ctrl"		, type = "copter/ctrl_copter"			},
		{ name = "ctrl_dump"	, type = "control/dump_ctrl"			},
		{ name = "state_dump"	, type = "control/dump_state"			},
		{ name = "kf_pred_cptr"	, type = "copter/kf_predict_copter"		},
		{ name = "tgj_gen"	, type = "copter/prim_trj_gen"		},
		{ name = "graph_dump"	, type = "copter/copter_graphic_dump"		},
		{ name = "timing"	, type = "control/dump_time_stat"		},
	},

	connections = {
		{ src="ctrl.ctrl_action"	, tgt="ctrl_action"		},
		{ src="ctrl.ctrl_action" 	, tgt="ctrl_dump"		},
		{ src="state"	 		, tgt="ctrl.state"		},
		{ src="setpoint"	 	, tgt="ctrl.setpoint"		},

		{ src="kf_pred_cptr.stt_out"	, tgt="state"			},
		{ src="kf_pred_cptr.stt_out"	, tgt="state_dump"		},
		{ src="state"			, tgt="kf_pred_cptr.stt_in"	},
		{ src="ctrl_action"		, tgt="kf_pred_cptr.ctrl_in"	},

		{ src="tgj_gen.setpoint_out"	, tgt="setpoint"		},
		{ src="state"			,tgt="tgj_gen.state_in"		},

		{ src="state"			, tgt="graph_dump.state"	},
		{ src="setpoint"		, tgt="graph_dump.setpoint"	},
	},

	configurations = {
		{
			name="kf_pred_cptr" ,
			config = {
				mass		= 1 	,
				J_xx		= 0.01,
				J_yy		= 0.01,
				J_zz		= 0.01,
				dt		= 0.003333 ,
				process_noise	= 0	,
			 },
		},
		{
			name="ctrl_action" ,
			config = {
				shm_name="/ctrl_action" ,
				type_name="struct ControlAction",
			 },
		},
		{
			name="setpoint" ,
			config = {
				shm_name="/setpoint" ,
				type_name="struct SetPoint",
			 },
		},
		{
			name="state" ,
			config = {
				shm_name="/state" ,
				type_name="struct State",
			 },
		},
		{
			name="tgj_gen" ,
			config = {
				dt	= 0.003333	;
			 },
		},
		{
			name="ctrl" ,
			config = {
				mass		= 1	,
				inertia_xx	= 0.01,
				inertia_yy	= 0.01,
				inertia_zz	= 0.01,

				k1		= 0.7	,--0.9	,
				l1		= 5	,--5	,	-- i.e. max speed for static set-point

				k2		= 10	,--8	,
				l2		= 10	,--8	,

				kp		= 40	,
				kd		= 10	,
				hysteresis_thr	= 0.01	,
				max_thrust	= 20	,
				max_torque_xx	= 3	,
				max_torque_yy	= 3	,
				max_torque_zz	= 3	,
			 },
		},
		{
			name="timing" ,
			config = {
				period		= 60*1000	;
				update_rate	= 0.001		;
			},

		},
		{
			name="ptrig1" ,
			config = {
				period = {sec=0, usec=3333 },
				trig_blocks={
					{ b="#ctrl"		, num_steps=1, measure=0 },
					{ b="#kf_pred_cptr"	, num_steps=1, measure=0 },
					{ b="#tgj_gen"		, num_steps=1, measure=0 },
					{ b="#timing"		, num_steps=1, measure=0 },
				},
			},

		},
		{
			name="graph_dump" ,
			config = {
				zoom = 5 ;
				max_q = 2 ;
			},

		},
		{
			name="ptrig_graphic" ,
			config = {
				period = {sec=0, usec=10000 },
				trig_blocks={
					{ b="#graph_dump"	, num_steps=1, measure=0 },
				},
			},

		},
	},

}


