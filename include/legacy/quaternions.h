/* Conjugate of a quaternion */
#define CONJUGATE_Q(d,s) {		\
	(d)[0] = +(s)[0] ;		\
	(d)[1] = -(s)[1] ;		\
	(d)[2] = -(s)[2] ;		\
	(d)[3] = -(s)[3] ;		\
}
/* Hamilton product of 2 quaternions */
#define HAMILTON(c,a,b)		{								\
	(c)[0] = (a)[0]*(b)[0] - (a)[1]*(b)[1] - (a)[2]*(b)[2] - (a)[3]*(b)[3] ; 	\
	(c)[1] = (a)[0]*(b)[1] + (a)[1]*(b)[0] + (a)[2]*(b)[3] - (a)[3]*(b)[2] ; 	\
	(c)[2] = (a)[0]*(b)[2] - (a)[1]*(b)[3] + (a)[2]*(b)[0] + (a)[3]*(b)[1] ; 	\
	(c)[3] = (a)[0]*(b)[3] + (a)[1]*(b)[2] - (a)[2]*(b)[1] + (a)[3]*(b)[0] ; 	\
}
/* Rotate vector apply rotation q to s and store result in d */
/* === NB === s and d are in the form [0 x y z]' !!! */
/* TODO!!! Re-implement this function!! */
#define ROTATE_BY_Q(d,s,q)	{										\
	float tmp_q_c[4];												\
	tmp_q_c[0] = (q)[0]*(s)[0] - (q)[1]*(s)[1] - (q)[2]*(s)[2] - (q)[3]*(s)[3] ;	 			\
	tmp_q_c[1] = (q)[0]*(s)[1] + (q)[1]*(s)[0] + (q)[2]*(s)[3] - (q)[3]*(s)[2] ; 				\
	tmp_q_c[2] = (q)[0]*(s)[2] - (q)[1]*(s)[3] + (q)[2]*(s)[0] + (q)[3]*(s)[1] ; 				\
	tmp_q_c[3] = (q)[0]*(s)[3] + (q)[1]*(s)[2] - (q)[2]*(s)[1] + (q)[3]*(s)[0] ; 				\
	(d)[0] =  (tmp_q_c)[0]*(q)[0] + (tmp_q_c)[1]*(q)[1] + (tmp_q_c)[2]*(q)[2] + (tmp_q_c)[3]*(q)[3] ; 	\
	(d)[1] = -(tmp_q_c)[0]*(q)[1] + (tmp_q_c)[1]*(q)[0] - (tmp_q_c)[2]*(q)[3] + (tmp_q_c)[3]*(q)[2] ; 	\
	(d)[2] = -(tmp_q_c)[0]*(q)[2] + (tmp_q_c)[1]*(q)[3] + (tmp_q_c)[2]*(q)[0] - (tmp_q_c)[3]*(q)[1] ; 	\
	(d)[3] = -(tmp_q_c)[0]*(q)[3] - (tmp_q_c)[1]*(q)[2] + (tmp_q_c)[2]*(q)[1] + (tmp_q_c)[3]*(q)[0] ; 	\
}

/* Apply rodriguez formula to convert quaternion q to rotation matrix m */
#define RODRIGUEZ(m,q){						\
	(m)[0][0] = 1 - 2*(q)[2]*(q)[2] - 2*(q)[3]*(q)[3]		\
	(m)[0][1] =     2*(q)[1]*(q)[2] - 2*(q)[3]*(q)[0]		\
	(m)[0][2] =     2*(q)[1]*(q)[3] + 2*(q)[2]*(q)[0]		\
								\
	(m)[1][0] =     2*(q)[1]*(q)[2] + 2*(q)[3]*(q)[0]		\
	(m)[1][1] = 1 - 2*(q)[1]*(q)[1] - 2*(q)[3]*(q)[3]		\
	(m)[1][2] =     2*(q)[2]*(q)[3] - 2*(q)[1]*(q)[0]		\
								\
	(m)[2][0] =     2*(q)[1]*(q)[3] - 2*(q)[2]*(q)[0]		\
	(m)[2][1] =     2*(q)[2]*(q)[3] + 2*(q)[1]*(q)[0]		\
	(m)[2][2] = 1 - 2*(q)[1]*(q)[1] - 2*(q)[2]*(q)[2]		\
}









