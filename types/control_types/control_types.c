/*
 * types for the autopilot
 */

#include <stdint.h>
#include <ubx/ubx.h>

#include "State.h"
#include "State.h.hexarr"
#include "ControlAction.h"
#include "ControlAction.h.hexarr"
#include "SensorData.h"
#include "SensorData.h.hexarr"
#include "SetPoint.h"
#include "SetPoint.h.hexarr"
#include "LinearSystem.h"
#include "LinearSystem.h.hexarr"

ubx_type_t copter_types[] = {

//	def_struct_type ( struct Matrix		, &Matrix_h		),
//	def_struct_type ( struct Vector		, &Vector_h		),
	def_struct_type ( struct State		, &State_h		),
	def_struct_type ( struct SensorData	, &SensorData_h		),
	def_struct_type ( struct SetPoint	, &SetPoint_h		),
	def_struct_type ( struct ControlAction	, &ControlAction_h	),
	def_struct_type ( struct LinearSystem	, &LinearSystem_h	),
	{ NULL }
};

static int type_init(ubx_node_info_t* ni)
{
	ubx_type_t *tptr;
	for(tptr=copter_types; tptr->name!=NULL; tptr++) {
		/* TODO check for errors */
		ubx_type_register(ni, tptr);
	}

	return 0;
}

static void type_cleanup(ubx_node_info_t *ni)
{
	const ubx_type_t *tptr;
	for(tptr=copter_types; tptr->name!=NULL; tptr++)
		ubx_type_unregister(ni, tptr->name);
}

UBX_MODULE_INIT(type_init)
UBX_MODULE_CLEANUP(type_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)

