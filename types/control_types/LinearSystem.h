struct LinearSystem{
	char		metadata	[128]		;
	// State transition mat	
	struct {
	  double	data		[1024];  
  	  unsigned int	n_rows;
	  unsigned int	n_cols;
	} A ;
	// Input model
	struct {
	  double	data		[1024];  
  	  unsigned int	n_rows;
	  unsigned int	n_cols;
	} B ;
	// System noise
	struct {
	  double	data		[1024];  
  	  unsigned int	n_rows;
	  unsigned int	n_cols;
	} Q ;
};

