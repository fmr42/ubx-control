
#include <sstream>
#include <string>
#include <iostream>
#include <vector>

#include <armadillo>

using namespace std;
using namespace arma;

void print_usage(){
	cout << "Usage:\n  create_mat_file <#rows> <#cols> <filename>\n";
	return;
}

int main(int argc, char **argv) {
	
	if ( argc!=4 ) {
	  print_usage();
	  return(-1);
	}

	mat M;
	int i;
	int j;
	std::string tmp;
	std::vector<double> nums;
	std::stringstream ss(tmp);
	double ti;
	std::vector<double>::iterator it;	

	int rows;
	int cols;

	// Read config
	rows=atof(argv[1]);
	cols=atof(argv[2]);

	if ( rows==0 or cols==0 ){
	  print_usage();
	  return 0;
	}
	

	M.zeros(rows,cols);
	// Read first line to fix number of colums
			
	// Read all other lines	
	for(i=0;i<rows;i++){
	  string str;
	  if (!getline(cin, str)) {
	    cout << "Invalid input\n";
	    return -1;
	  }
	  istringstream ss(str);
	  double f;
	  for(j=0;j<cols;j++){
	    if (!(ss >> f)) {
	      cout << "Invalid input\n";
	      return -1;
	    }
	    M(i,j)=f;
	  }
	}

	cout << endl;
	M.print(argv[3]);
	cout << endl;
// TODO chose right format
// check at http://arma.sourceforge.net/docs.html#save_load_mat
	cout << "Saving matrix... ";
	bool status = M.save(argv[3],arma_ascii);
	
	
	if(status == true) {
	  cout << "DONE" << endl;
	  return (0);
	} else {
	  cout << "Check file name and permissions." << endl;
	  return(-1);
	}


}
	
	



