#include <gtk/gtk.h>
#include <mqueue.h>

mqd_t mq;  //mq
/* Callback functions */
static void go_n ( GtkWidget *widget , gpointer data) {
	int msg=1;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void go_s ( GtkWidget *widget , gpointer data) {
	int msg=2;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void go_w ( GtkWidget *widget , gpointer data) {
	int msg=3;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void go_e ( GtkWidget *widget , gpointer data) {
	int msg=4;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void go_u ( GtkWidget *widget , gpointer data) {
	int msg=5;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void go_d ( GtkWidget *widget , gpointer data) {
	int msg=6;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void yaw_plus ( GtkWidget *widget , gpointer data) {
	int msg=7;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void yaw_minus ( GtkWidget *widget , gpointer data) {
	int msg=8;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}
static void stop ( GtkWidget *widget , gpointer data) {
	int msg=0;
	int r = mq_send(mq, (char *)(&msg), sizeof(int), 1);
}

static gboolean
on_delete_event (GtkWidget *widget,
                 GdkEvent  *event,
                 gpointer   data)
{
mq_close(mq);
  return FALSE;
}

int main (int   argc , char *argv[]) {
  struct mq_attr ma;      // message queue attributes
  ma.mq_flags = 0;                // blocking read/write
  ma.mq_maxmsg = 10;              // maximum number of messages allowed in queue
  ma.mq_msgsize = sizeof(int);    // messages are contents of an int
  ma.mq_curmsgs = 0;              // number of messages currently in queue
  mq = mq_open("/test", O_RDWR | O_CREAT, 0700, &ma);

  if (mq == -1) {
    printf("Failed to create queue.\n");
    return(-1);
  }

  GtkWidget *window;
  GtkWidget *button_n;
  GtkWidget *button_s;
  GtkWidget *button_w;
  GtkWidget *button_e;
  GtkWidget *button_u;
  GtkWidget *button_d;
  GtkWidget *button_yaw_plus;
  GtkWidget *button_yaw_minus;

  GtkWidget *button_stop;

  gtk_init (&argc, &argv);

	// Draw the window
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 250, 180);
  gtk_window_set_title (GTK_WINDOW (window), "Drive the copter!");
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);

	// Create buttons
  button_n = gtk_button_new_with_label ("N");
  button_s = gtk_button_new_with_label ("S");
  button_w = gtk_button_new_with_label ("W");
  button_e = gtk_button_new_with_label ("E");
  button_u = gtk_button_new_with_label ("U");
  button_d = gtk_button_new_with_label ("D");
  button_yaw_plus = gtk_button_new_with_label ("y++");
  button_yaw_minus = gtk_button_new_with_label ("y--");
  button_stop = gtk_button_new_with_label ("stp");
	//
GtkWidget *grid = gtk_grid_new();

gtk_grid_attach (GTK_GRID (grid), button_n, 1, 0, 1, 1);
gtk_grid_attach (GTK_GRID (grid), button_s, 1, 2, 1, 1);
gtk_grid_attach (GTK_GRID (grid), button_w, 0, 1, 1, 1);
gtk_grid_attach (GTK_GRID (grid), button_e, 2, 1, 1, 1);
gtk_grid_attach (GTK_GRID (grid), button_u, 3, 0, 1, 1);
gtk_grid_attach (GTK_GRID (grid), button_d, 3, 2, 1, 1);

gtk_grid_attach (GTK_GRID (grid), button_yaw_plus, 4, 0, 1, 1);
gtk_grid_attach (GTK_GRID (grid), button_yaw_minus, 4, 2, 1, 1);

gtk_grid_attach (GTK_GRID (grid), button_stop, 1, 1, 1, 1);

  gtk_container_add(GTK_CONTAINER(window), grid);

	// Connect the signals
  g_signal_connect (window, "delete-event", G_CALLBACK (on_delete_event), NULL);
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

  g_signal_connect (button_n, "clicked", G_CALLBACK (go_n)	, NULL);
  g_signal_connect (button_s, "clicked", G_CALLBACK (go_s)	, NULL);
  g_signal_connect (button_w, "clicked", G_CALLBACK (go_w)	, NULL);
  g_signal_connect (button_e, "clicked", G_CALLBACK (go_e)	, NULL);
  g_signal_connect (button_u, "clicked", G_CALLBACK (go_u)	, NULL);
  g_signal_connect (button_d, "clicked", G_CALLBACK (go_d)	, NULL);

  g_signal_connect (button_yaw_plus, "clicked", G_CALLBACK (yaw_plus)	, NULL);
  g_signal_connect (button_yaw_minus, "clicked", G_CALLBACK (yaw_minus)	, NULL);

  g_signal_connect (button_stop, "clicked", G_CALLBACK (stop)	, NULL);
 
  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}
